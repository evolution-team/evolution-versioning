# Evolution - Versioning
Versioning solution for hibernate entities.

## Setup
Add package `cz.cvut.fit.evolution.versioning.model` scan location to your entity manager (`packagesToScan` property)

```xml
<bean id="entityManagerFactory" class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
    <property name="dataSource" ref="mainDataSource"/>
    <property name="persistenceProvider" ref="hibernatePersistence"/>
    <property name="jpaProperties" ref="jpaProperties"/>
    <property name="packagesToScan" value="your.package,cz.cvut.fit.evolution.versioning.model"/>
</bean>
```

Import versioning services to your spring-context.
```xml
<import resource="classpath:versioning-services.xml"/>
```

Choose the following annotation setting appropriately:
* `@Versioned` - adds versioning support to the entity

```java
@Entity
@Versioned
public class MyDatabaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String name;
    
    // ...
}
```
<!--
* `@EntityListeners(VersionedListener.class)` - will automatically save new version when hibernate commits this entity to the database (should be also `@Versioned`)
```java
@EntityListeners(VersionedListener.class)
```
--> 


## Usage

Call the service to save your entity.

```java
versionService.saveHistoryEntry(entity);
```

Check out the test in this project for more examples.

#### Few things to keep in mind:
* All entities should have hibernate `@Id` annotation as primitive type, i.e., `String, Long, Integer, Boolean, Double, Float`.
* History of entities without `@Versioned` will not be saved. When these entities are searched for the non-history (current) version is returned. This happens also when the entity is referenced from a versioned entity.
* Bulk operations together to perform less queries to the db.

## How does it work
* You send entities to be saved.
* Parameters of these entities are searched, they will be versioned as well.
* Search for history versions of all currently saved entities.
* Compare history versions with current versions and save what changed.

The parameters and lists are versioned in the same way.
The difference is only in number of saved HistoryParameters.
Its value is internally saved in the following way. 
* primitive - by value
* enum - by name()
* non-versioned entity - by id value (must be primitive)
* versioned entity - by id value (must be primitive), however if the parameter has `@VersionedParameter` annotation, the saving will be propagated
