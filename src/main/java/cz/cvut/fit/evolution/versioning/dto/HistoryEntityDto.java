package cz.cvut.fit.evolution.versioning.dto;


import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.HistoryParameter;
import cz.cvut.fit.evolution.versioning.utility.GeneralUtil;
import cz.cvut.fit.evolution.versioning.utility.PrimitiveTypes;
import cz.cvut.fit.evolution.versioning.utility.TimeUtil;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class HistoryEntityDto implements HistoryAbstractDto<HistoryEntity> {

    private Long historyEntityId;

    private Object entityId;

    private String clazz;

    private List<HistoryParameterDto> parameters = new ArrayList<>();

    private String author;

    private DateTime created;

    private DateTime deleted;


    public HistoryEntityDto() {
    }

    public HistoryEntityDto(Long entityId, String clazz, String author, DateTime created, DateTime deleted) {
        this.entityId = entityId;
        this.clazz = clazz;
        this.author = author;
        this.created = created;
        this.deleted = deleted;
    }

    @Override
    public void fromEntity(HistoryEntity entity) {
        fromEntity(entity, null);
    }

    void fromEntity(HistoryEntity entity, HistoryParameterDto parameter) {
        setHistoryEntityId(entity.getHistoryEntityId());
        try {
            setEntityId(PrimitiveTypes.convert(Class.forName(entity.getClazz()), entity.getEntityId()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        setClazz(entity.getClazz());
        final Long parameterId = parameter != null ? parameter.getHistoryParameterId() : null;
        final List<HistoryParameterDto> parameterDtos = entity.getParameters().stream().map(historyParameter -> {
            final Long historyId = historyParameter.getHistoryParameterId();
            HistoryParameterDto dto = parameter != null && parameterId.equals(historyId) ? parameter : new HistoryParameterDto();
            dto.fromEntity(historyParameter, this);
            return dto;
        }).collect(Collectors.toList());
        setParameters(parameterDtos);
        setAuthor(entity.getAuthor());
        setCreated(TimeUtil.timeFromTimestamp(entity.getCreated()));
        setDeleted(TimeUtil.timeFromTimestamp(entity.getDeleted()));
    }

    @Override
    public HistoryEntity toEntity() {
        return toEntity(null);
    }

    public HistoryEntity toEntity(HistoryParameter parameter) {
        final HistoryEntity historyEntity = new HistoryEntity();
        historyEntity.setHistoryEntityId(getHistoryEntityId());
        historyEntity.setEntityIdClass(getEntityId().getClass().getCanonicalName());
        historyEntity.setEntityId(PrimitiveTypes.convert(getEntityId()));
        historyEntity.setClazz(getClazz());
        final Long parId = parameter != null ? parameter.getHistoryParameterId() : null;
        final List<HistoryParameter> parameters = getParameters().stream().map(historyParameterDto -> {
            final Long histId = historyParameterDto.getHistoryParameterId();
            HistoryParameter histPar = parameter != null && histId.equals(parId) ? parameter : new HistoryParameter();
            return historyParameterDto.toEntity(histPar, historyEntity);
        }).collect(Collectors.toList());
        historyEntity.setParameters(parameters);
        historyEntity.setAuthor(getAuthor());
        historyEntity.setCreated(TimeUtil.timeToTimestamp(getCreated()));
        historyEntity.setDeleted(TimeUtil.timeToTimestamp(getDeleted()));
        return historyEntity;
    }


    public Long getHistoryEntityId() {
        return historyEntityId;
    }

    public void setHistoryEntityId(Long historyEntityId) {
        this.historyEntityId = historyEntityId;
    }

    public Object getEntityId() {
        return entityId;
    }

    public void setEntityId(Object entityId) {
        this.entityId = entityId;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public List<HistoryParameterDto> getParameters() {
        return parameters;
    }

    public void setParameters(List<HistoryParameterDto> parameters) {
        this.parameters = parameters;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(DateTime deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "HistoryEntityDto{" +
                "historyEntityId=" + historyEntityId +
                ", entityId=" + entityId +
                ", clazz='" + clazz + '\'' +
                ", parameters=" + parameters +
                ", author='" + author + '\'' +
                ", created=" + created +
                ", deleted=" + deleted +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoryEntityDto that = (HistoryEntityDto) o;

        if (historyEntityId != null ? !historyEntityId.equals(that.historyEntityId) : that.historyEntityId != null)
            return false;
        if (entityId != null ? !entityId.equals(that.entityId) : that.entityId != null) return false;
        if (clazz != null ? !clazz.equals(that.clazz) : that.clazz != null) return false;
        if (!GeneralUtil.listEq(parameters, that.parameters)) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        return deleted != null ? deleted.equals(that.deleted) : that.deleted == null;
    }

    @Override
    public int hashCode() {
        int result = historyEntityId != null ? historyEntityId.hashCode() : 0;
        result = 31 * result + (entityId != null ? entityId.hashCode() : 0);
        result = 31 * result + (clazz != null ? clazz.hashCode() : 0);
        result = 31 * result + (parameters != null ? parameters.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        return result;
    }
}