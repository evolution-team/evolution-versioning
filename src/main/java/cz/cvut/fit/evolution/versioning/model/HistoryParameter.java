package cz.cvut.fit.evolution.versioning.model;

import cz.cvut.fit.evolution.versioning.utility.TimeUtil;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class HistoryParameter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long historyParameterId;

    private String parameterName;

    private String textValue;

    private String type;

    private String author;

    @ManyToOne(fetch=FetchType.LAZY)
    private HistoryEntity historyEntity;

    private Timestamp created;

    private Timestamp deleted;

    public HistoryParameter() {
    }

    public HistoryParameter(String parameterName, String textValue, String type, String author, HistoryEntity historyEntity, Timestamp created, Timestamp deleted) {
        this.parameterName = parameterName;
        this.textValue = textValue;
        this.type = type;
        this.author = author;
        this.historyEntity = historyEntity;
        this.created = created;
        this.deleted = deleted;
    }

    public Long getHistoryParameterId() {
        return historyParameterId;
    }

    public void setHistoryParameterId(Long historyParameterId) {
        this.historyParameterId = historyParameterId;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public HistoryEntity getHistoryEntity() {
        return historyEntity;
    }

    public void setHistoryEntity(HistoryEntity historyEntity) {
        this.historyEntity = historyEntity;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getDeleted() {
        return deleted;
    }

    public void setDeleted(Timestamp deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "HistoryParameter{" +
                "historyParameterId=" + historyParameterId +
                ", parameterName='" + parameterName + '\'' +
                ", textValue='" + textValue + '\'' +
                ", type='" + type + '\'' +
                ", author='" + author + '\'' +
                ", historyEntityId=" + (historyEntity != null ? historyEntity.getHistoryEntityId() : "") +
                ", created=" + created +
                ", deleted=" + deleted +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoryParameter that = (HistoryParameter) o;

        if (historyParameterId != null ? !historyParameterId.equals(that.historyParameterId) : that.historyParameterId != null)
            return false;
        if (parameterName != null ? !parameterName.equals(that.parameterName) : that.parameterName != null)
            return false;
        if (textValue != null ? !textValue.equals(that.textValue) : that.textValue != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (historyEntity == null && that.historyEntity != null)
            return false;
        if (historyEntity != null && that.historyEntity != null) {
            if (historyEntity.getHistoryEntityId() != null
                    ? !historyEntity.getHistoryEntityId().equals(that.historyEntity.getHistoryEntityId())
                    : that.historyEntity.getHistoryEntityId() != null)
                return false;
        }
        if (created != null ? !TimeUtil.same(created, that.created) : that.created != null) return false;
        return deleted != null ? deleted.equals(that.deleted) : that.deleted == null;
    }

    @Override
    public int hashCode() {
        int result = historyParameterId != null ? historyParameterId.hashCode() : 0;
        result = 31 * result + (parameterName != null ? parameterName.hashCode() : 0);
        result = 31 * result + (textValue != null ? textValue.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (historyEntity != null ? historyEntity.getHistoryEntityId() != null ? historyEntity.getHistoryEntityId().hashCode() : 0 : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        return result;
    }
}