package cz.cvut.fit.evolution.versioning.utility;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class PrimitiveTypes {

    private static final Map<Class, Function<String, Object>> TYPES;

    static {
        TYPES = new HashMap<>();
        TYPES.put(String.class, s -> s);
        TYPES.put(Long.class, Long::valueOf);
        TYPES.put(Integer.class, Integer::valueOf);
        TYPES.put(Boolean.class, Boolean::valueOf);
        TYPES.put(Double.class, Double::valueOf);
        TYPES.put(Float.class, Float::valueOf);
    }

    public static Object convert(Class<?> clazz, String value) {
        return TYPES.get(clazz).apply(value);
    }

    public static String convert(Object value) {
        return value.toString();
    }

    public static boolean contain(Class<?> clazz) {
        return TYPES.containsKey(clazz);
    }

    public static void add(Class<?> clazz, Function<String, Object> converter) {
        TYPES.put(clazz, converter);
    }
}
