package cz.cvut.fit.evolution.versioning.utility;

import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.HistoryParameter;

import java.util.ArrayList;
import java.util.List;

public class ComplexHistoryEntity {
    private Class<?> clazz;
    private Object entityId;
    private HistoryEntity historyEntity;
    private List<HistoryParameter> historyParameters;

    public ComplexHistoryEntity() {
        historyParameters = new ArrayList<>();
    }

    public ComplexHistoryEntity(Class<?> clazz, Object id, HistoryEntity historyEntity, List<HistoryParameter> historyParameters) {
        this.clazz = clazz;
        this.entityId = id;
        this.historyEntity = historyEntity;
        this.historyParameters = historyParameters;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public Object getEntityId() {
        return entityId;
    }

    public void setEntityId(Object entityId) {
        this.entityId = entityId;
    }

    public HistoryEntity getHistoryEntity() {
        return historyEntity;
    }

    public void setHistoryEntity(HistoryEntity historyEntity) {
        this.historyEntity = historyEntity;
    }

    public List<HistoryParameter> getHistoryParameters() {
        return historyParameters;
    }

    public void setHistoryParameters(List<HistoryParameter> historyParameters) {
        this.historyParameters = historyParameters;
    }
}
