package cz.cvut.fit.evolution.versioning.service.impl;

import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.HistoryParameter;
import cz.cvut.fit.evolution.versioning.service.MigrateVersionService;
import cz.cvut.fit.evolution.versioning.service.RawVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Field;
import java.util.List;

public class MigrateVersionServiceImpl implements MigrateVersionService {

    @PersistenceContext(unitName = "appPU")
    @Qualifier(value = "entityManagerFactory")
    private EntityManager entityManager;

    @Autowired
    private RawVersionService rawVersionService;


    @Override
    @Transactional
    public <T> void renameParameter(Class<T> clazz, String from, String to) {
        final List<HistoryEntity> entities = rawVersionService.getAllHistoryEntities(clazz);
        for (HistoryEntity entity : entities) {
            final List<HistoryParameter> parameters = entity.getParameters();
            for (HistoryParameter parameter : parameters) {
                if (parameter.getParameterName().equals(from)) {
                    parameter.setParameterName(to);
                }
            }
        }
    }

    @Override
    @Transactional
    public <T> void changeParameterType(Class<T> clazz, String parameterName, Class to) {
        final List<HistoryEntity> entities = rawVersionService.getAllHistoryEntities(clazz);
        for (HistoryEntity entity : entities) {
            final List<HistoryParameter> parameters = entity.getParameters();
            for (HistoryParameter parameter : parameters) {
                final String name = parameter.getParameterName();
                if (name.equals(parameterName)) {
                    parameter.setType(to.getCanonicalName());
                }
            }
        }
    }


    @Override
    @Transactional
    public <T> void changeParameterValue(Class<T> clazz, String parameterName, T originalValue, T newValue) {
        final HistoryParameter pattern = new HistoryParameter();
        pattern.setType(clazz.getCanonicalName());
        pattern.setParameterName(parameterName);
        pattern.setTextValue(originalValue.toString());

        final HistoryParameter target = new HistoryParameter();
        target.setTextValue(newValue.toString());

        changeParameter(pattern, target);
    }

    @Override
    @Transactional
    public void migrateClass(Class from, Class to) {
        final List<HistoryEntity> entities = rawVersionService.getAllHistoryEntities(from);
        for (HistoryEntity entity : entities) {
            final String fromName = from.getCanonicalName();
            final String toName = to.getCanonicalName();
            if (entity.getClazz().equals(fromName)) {
                entity.setClazz(toName);
            }
            final List<HistoryParameter> parameters = entity.getParameters();
            for (HistoryParameter parameter : parameters) {
                if (parameter.getType().equals(fromName)) {
                    parameter.setType(toName);
                }
            }
        }
    }

    @Override
    @Transactional
    public void changeParameter(HistoryParameter pattern, HistoryParameter target) {
        try {
            final List<HistoryParameter> allHistoryParameters = rawVersionService.getAllHistoryParameters();
            for (HistoryParameter parameter : allHistoryParameters) {
                boolean pass = true;
                for (Field field : parameter.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    final Object patternValue = field.get(pattern);
                    final Object value = field.get(parameter);
                    if (patternValue != null && !patternValue.equals(value)) {
                        pass = false;
                        break;
                    }
                    field.setAccessible(false);
                }
                if (pass) {
                    for (Field field : parameter.getClass().getDeclaredFields()) {
                        field.setAccessible(true);
                        final Object targetValue = field.get(target);
                        if (targetValue != null) {
                            field.set(parameter, targetValue);
                            entityManager.persist(parameter);
                        }
                        field.setAccessible(false);
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
