package cz.cvut.fit.evolution.versioning.utility;

import java.util.HashMap;
import java.util.Map;

public class QuickSearch<A, B, C> {
    private Map<A, Map<B, C>> saved = new HashMap<>();

    public C fetch(A a, B b) {
        if (saved.containsKey(a) && saved.get(a).containsKey(b)) {
            return saved.get(a).get(b);
        }
        return null;
    }

    public void save(A a, B b, C c) {
        final Map<B, C> ids = saved.computeIfAbsent(a, k -> new HashMap<>());
        ids.put(b, c);
    }
}
