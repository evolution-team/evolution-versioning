package cz.cvut.fit.evolution.versioning.service;


import cz.cvut.fit.evolution.versioning.Versioned;
import org.joda.time.DateTime;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * The VersionService allows to save and query history versions of database entities.
 * <p>
 * All entities with versioned history should have the {@link Versioned} annotation.
 *
 * @author Václav Blažej
 * @see Versioned
 * @since 1.0
 */
public interface VersionService {

    /**
     * Fetches the current history version of entity.
     * <p>
     * The class of the entity is required to have the {@link Versioned} annotation, otherwise this method returns null.
     *
     * @param <T>   class type of the entity
     * @param clazz class of the entity
     * @param id    id if the entity
     * @return history version of the entity at time given by timestamp
     */
    <T> T getHistoryVersion(Class<T> clazz, Object id);

    /**
     * Fetches version of entity from any time in it's tracked history.
     * <p>
     * The class of the entity is required to have the {@link Versioned} annotation, otherwise this method returns null.
     *
     * @param <T>       class type of the entity
     * @param clazz     class of the entity
     * @param id        id if the entity
     * @param timestamp time in the entity history from which it should be returned
     * @return history version of the entity at time given by timestamp
     */
    <T> T getHistoryVersion(Class<T> clazz, Object id, DateTime timestamp);

    /**
     * Fetches all versions of entity throughout history.
     * <p>
     * The class of the entity is required to have the {@link Versioned} annotation, otherwise this method returns null.
     *
     * @param <T>   entity type
     * @param clazz class of the entity
     * @param id    id of the entity
     * @return sorted map of all history moments paired up with respective entity versions
     * @see Versioned
     */
    <T> Map<DateTime, T> getAllHistoryVersions(Class<T> clazz, Long id);

    <T> List<DateTime> getAllHistoryVersionTime(Class<T> clazz, Long id);

    <T> void saveEntityChanges(Collection<T> objects, String author);

    /**
     * Will save the current entity state.
     * Assumes, that the unique id of this entity is annotated with {@link javax.persistence.Id}.
     *
     * @param <T>    class of the instance
     * @param entity instance to be saved
     * @param author
     * @return a unique identifier of the history entry used to track the entity throughout the history
     */
    <T> void saveEntityChanges(T entity, String author);

    /**
     * Will save the current entity state.
     *
     * @param <T>    class of the instance
     * @param entity instance to be saved
     * @param id     id of the instance
     * @param author
     * @return a unique identifier of the history entry used to track the entity throughout the history
     */
    <T> void saveEntityChanges(T entity, Object id, String author);

    <T> void saveHistoryEntry(Collection<T> objects, String author);

    <T> void saveHistoryEntry(T entity, String author);

    <T> void saveHistoryEntry(T entity, Object id, String author);

    <T> void saveEntityChanges(T entity, Object id, Class<? extends T> clazz, String author);

    <T> void deleteHistoryEntry(Object id, Class<T> clazz, String author);
}
