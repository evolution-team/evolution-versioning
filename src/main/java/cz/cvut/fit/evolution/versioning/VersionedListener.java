package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.service.VersionService;
import cz.cvut.fit.evolution.versioning.utility.AutowireHelper;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.persistence.*;

/**
 * The class VersionedListener allows for automatic versioning invocation.
 * <p>
 * To setup entity for automatic history tracking, setup it as a hibernate listener by adding following in front of the entity.
 * <p>
 * <code>@{@link EntityListeners}(VersionedListener.class)</code>
 *
 * @author Václav Blažej
 * @see Versioned
 * @since 1.0
 */
public class VersionedListener {

    @PersistenceContext(unitName = "appPU")
    @Qualifier(value = "entityManagerFactory")
    private EntityManager entityManager;

    @Autowired
    private VersionService versionService;

    @PostPersist
    @PostUpdate
    @PostRemove
    private void prePersist(Object entity) {
        AutowireHelper.autowire(this);
        final Session session = entityManager.unwrap(Session.class);
        session.setFlushMode(FlushMode.MANUAL);
        versionService.saveEntityChanges(entity, "system");
        session.setFlushMode(FlushMode.AUTO);
    }

}
