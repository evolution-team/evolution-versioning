package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.service.VersionService;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation VersionedParameter is required to allow transitive history tracking for versioned entities.
 *
 * @author Václav Blažej
 * @see VersionService
 * @since 1.0
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface VersionedParameter {
}
