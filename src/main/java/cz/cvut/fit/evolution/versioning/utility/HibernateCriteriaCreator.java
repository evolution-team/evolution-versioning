package cz.cvut.fit.evolution.versioning.utility;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class HibernateCriteriaCreator {

    @PersistenceContext(unitName = "appPU")
    @Qualifier(value = "entityManagerFactory")
    private EntityManager entityManager;

    @Transactional
    public Criteria createCriteria(Class resultClass, String alias) {
        final HibernateEntityManager hibernateEntityManager = entityManager.unwrap(HibernateEntityManager.class);
        final Session session = hibernateEntityManager.getSession();
        return session.createCriteria(resultClass, alias);
    }
}
