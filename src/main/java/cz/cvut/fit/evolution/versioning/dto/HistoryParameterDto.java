package cz.cvut.fit.evolution.versioning.dto;


import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.HistoryParameter;
import cz.cvut.fit.evolution.versioning.utility.TimeUtil;
import org.joda.time.DateTime;

import static cz.cvut.fit.evolution.versioning.utility.TimeUtil.timeFromTimestamp;


public class HistoryParameterDto implements HistoryAbstractDto<HistoryParameter> {

    private Long historyParameterId;

    private String parameterName;

    private String textValue;

    private String type;

    private String author;

    private HistoryEntityDto historyEntity;

    private DateTime created;

    private DateTime deleted;


    public HistoryParameterDto() {
    }

    public HistoryParameterDto(String parameterName, String textValue, String type, String author, HistoryEntityDto entityDto, DateTime created, DateTime deleted) {
        this.parameterName = parameterName;
        this.textValue = textValue;
        this.type = type;
        this.author = author;
        this.historyEntity = entityDto;
        this.created = created;
        this.deleted = deleted;
    }

    @Override
    public void fromEntity(HistoryParameter entity) {
        setHistoryParameterId(entity.getHistoryParameterId());
        final HistoryEntityDto historyEntityDto = new HistoryEntityDto();
        historyEntityDto.fromEntity(entity.getHistoryEntity(), this);
    }

    public void fromEntity(HistoryParameter entity, HistoryEntityDto historyEntityDto) {
        setHistoryParameterId(entity.getHistoryParameterId());
        setParameterName(entity.getParameterName());
        setTextValue(entity.getTextValue());
        setType(entity.getType());
        setAuthor(entity.getAuthor());
        setHistoryEntity(historyEntityDto);
        setCreated(timeFromTimestamp(entity.getCreated()));
        setDeleted(timeFromTimestamp(entity.getDeleted()));
    }

    @Override
    public HistoryParameter toEntity() {
        final HistoryParameter historyParameter = new HistoryParameter();
        historyParameter.setHistoryParameterId(getHistoryParameterId());
        final HistoryEntity historyEntity = getHistoryEntity().toEntity(historyParameter);
        return toEntity(historyParameter, historyEntity);
    }

    public HistoryParameter toEntity(HistoryParameter historyParameter, HistoryEntity historyEntity) {
        historyParameter.setHistoryParameterId(getHistoryParameterId());
        historyParameter.setParameterName(getParameterName());
        historyParameter.setTextValue(getTextValue());
        historyParameter.setType(getType());
        historyParameter.setAuthor(getAuthor());
        historyParameter.setHistoryEntity(historyEntity);
        historyParameter.setCreated(TimeUtil.timeToTimestamp(getCreated()));
        historyParameter.setDeleted(TimeUtil.timeToTimestamp(getDeleted()));
        return historyParameter;
    }


    public Long getHistoryParameterId() {
        return historyParameterId;
    }

    public void setHistoryParameterId(Long historyParameterId) {
        this.historyParameterId = historyParameterId;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public HistoryEntityDto getHistoryEntity() {
        return historyEntity;
    }

    public void setHistoryEntity(HistoryEntityDto historyEntity) {
        this.historyEntity = historyEntity;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(DateTime deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "HistoryParameterDto{" +
                "historyParameterId=" + historyParameterId +
                ", parameterName='" + parameterName + '\'' +
                ", textValue='" + textValue + '\'' +
                ", type='" + type + '\'' +
                ", author='" + author + '\'' +
                ", historyEntity=" + (historyEntity != null ? historyEntity.getHistoryEntityId() : null) +
                ", created=" + created +
                ", deleted=" + deleted +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoryParameterDto that = (HistoryParameterDto) o;

        if (historyParameterId != null ? !historyParameterId.equals(that.historyParameterId) : that.historyParameterId != null)
            return false;
        if (parameterName != null ? !parameterName.equals(that.parameterName) : that.parameterName != null)
            return false;
        if (textValue != null ? !textValue.equals(that.textValue) : that.textValue != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (historyEntity == null && that.historyEntity != null)
            return false;
        if (historyEntity != null && that.historyEntity != null) {
            if (historyEntity.getHistoryEntityId() != null
                    ? !historyEntity.getHistoryEntityId().equals(that.historyEntity.getHistoryEntityId())
                    : that.historyEntity.getHistoryEntityId() != null)
                return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        return deleted != null ? deleted.equals(that.deleted) : that.deleted == null;
    }

    @Override
    public int hashCode() {
        int result = historyParameterId != null ? historyParameterId.hashCode() : 0;
        result = 31 * result + (parameterName != null ? parameterName.hashCode() : 0);
        result = 31 * result + (textValue != null ? textValue.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (historyEntity != null ? historyEntity.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        return result;
    }
}