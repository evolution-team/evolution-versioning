package cz.cvut.fit.evolution.versioning.service.impl;

import cz.cvut.fit.dbcore.service.AbstractModelService;
import cz.cvut.fit.evolution.versioning.Versioned;
import cz.cvut.fit.evolution.versioning.VersionedParameter;
import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.HistoryParameter;
import cz.cvut.fit.evolution.versioning.service.RawVersionService;
import cz.cvut.fit.evolution.versioning.service.VersionService;
import cz.cvut.fit.evolution.versioning.utility.ComplexHistoryEntity;
import cz.cvut.fit.evolution.versioning.utility.EvolutionVersioningException;
import cz.cvut.fit.evolution.versioning.utility.PrimitiveTypes;
import cz.cvut.fit.evolution.versioning.utility.QuickSearch;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cz.cvut.fit.evolution.versioning.utility.GeneralUtil.getAllDeclaredFields;
import static cz.cvut.fit.evolution.versioning.utility.GeneralUtil.getFieldType;
import static cz.cvut.fit.evolution.versioning.utility.TimeUtil.timeFromTimestamp;
import static cz.cvut.fit.evolution.versioning.utility.TimeUtil.timeToTimestamp;
import static java.lang.reflect.Modifier.isStatic;

public class VersionServiceImpl extends AbstractModelService implements VersionService {

    private static DateTime transactionTime = null;
    private static Transaction lastTransaction = null;

    @Autowired
    private RawVersionService rawVersionService;

    private DateTime getTime() {
        final Session unwrap = entityManager.unwrap(Session.class);
        final Transaction transaction = unwrap.getTransaction();
        if (lastTransaction != transaction) {
            transactionTime = DateTime.now();
            lastTransaction = transaction;
        }
        return transactionTime;
    }

    @Override
    @Transactional
    public <T> T getHistoryVersion(Class<T> clazz, Object id) {
        return getHistoryVersion(clazz, id, getTime());
    }

    @Override
    @Transactional
    public <T> T getHistoryVersion(Class<T> clazz, Object id, DateTime time) {
        if (!isVersioned(clazz)) return justGetThatEntity(clazz, id);
        final HistoryEntity historyEntity = rawVersionService.getHistoryEntity(clazz, id);
        if (historyEntity == null
                || (timeFromTimestamp(historyEntity.getDeleted()) != null
                && !timeFromTimestamp(historyEntity.getDeleted()).isAfter(time))) return null;
        // todo optimize to not ask for each moment in time
        final List<HistoryParameter> parameters = rawVersionService.getHistoryParameters(clazz, id, time);
        final Map<String, List<String>> realValues = joinParameters(parameters, HistoryParameter::getParameterName);
        try {
            final T instance = clazz.newInstance();
            for (Field field : getAllDeclaredFields(clazz)) {
                List<String> list = realValues.computeIfAbsent(field.getName(), k -> new ArrayList<>());
                setEntryField(instance, field, list, time);
            }
            return instance;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @Transactional
    public <T> Map<DateTime, T> getAllHistoryVersions(Class<T> clazz, Long id) {
        if (!isVersioned(clazz)) {
            final HashMap<DateTime, T> dateTimeTHashMap = new HashMap<>();
            final T value = justGetThatEntity(clazz, id);
            if (value != null) dateTimeTHashMap.put(DateTime.now(), value);
            return dateTimeTHashMap;
        }
        final HistoryEntity historyEntity = rawVersionService.getHistoryEntity(clazz, id);
        if (historyEntity == null) return null;
        final List<HistoryParameter> parameters = historyEntity.getParameters();
        final Set<Timestamp> timestamps = new HashSet<>();
        timestamps.addAll(parameters.stream().map(HistoryParameter::getCreated).collect(Collectors.toSet()));
        timestamps.addAll(parameters.stream().map(HistoryParameter::getDeleted).collect(Collectors.toSet()));
        timestamps.remove(null);
        Map<DateTime, T> entries = new TreeMap<>();
        for (Timestamp timestamp : timestamps) {
            final DateTime dateTime = timeFromTimestamp(timestamp);
            final T historyVersion = getHistoryVersion(clazz, id, dateTime);
            entries.put(dateTime, historyVersion);
        }
        return entries;
    }

    @Override
    @Transactional
    public <T> List<DateTime> getAllHistoryVersionTime(Class<T> clazz, Long id) {
        List<DateTime> entries = new ArrayList<>();
        final HistoryEntity historyEntity = rawVersionService.getHistoryEntity(clazz, id);
        if (historyEntity == null) return null;
        final List<HistoryParameter> parameters = historyEntity.getParameters();
        final Set<Timestamp> timestamps = new HashSet<>();
        timestamps.addAll(parameters.stream().map(HistoryParameter::getCreated).collect(Collectors.toSet()));
        timestamps.addAll(parameters.stream().map(HistoryParameter::getDeleted).collect(Collectors.toSet()));
        timestamps.remove(null);
        for (Timestamp timestamp : timestamps) {
            final DateTime dateTime = timeFromTimestamp(timestamp);
            entries.add(dateTime);
        }
        return entries;
    }

    // history changes

    @Override
    @Transactional
    public <T> void saveEntityChanges(Collection<T> objects, String author) {
        final Set<Entry> entries = toEntries(objects);
        saveEntityChanges(entries, DateTime.now(), new QuickSearch(), author);
    }

    @Override
    @Transactional
    public <T> void saveEntityChanges(T entity, String author) {
        if (entity == null) {
            throw new EvolutionVersioningException("saveEntityChanges(T entity) cannot accept null" +
                    " as entity parameter since there is no way of determining its class," +
                    " use saveEntityChanges(T entity, Long id, Class<T> clazz) instead");
        }
        final Set<Entry> entries = new HashSet<>();
        entries.add(new Entry(entity));
        saveEntityChanges(entries, getTime(), new QuickSearch(), author);
    }

    @Override
    @Transactional
    public <T> void saveEntityChanges(T entity, Object id, String author) {
        if (entity == null) {
            throw new EvolutionVersioningException("saveEntityChanges(T entity, Long id) cannot accept null" +
                    " as entity parameter since there is no way of determining its class," +
                    " use saveEntityChanges(T entity, Long id, Class<T> clazz) instead");
        }
        final Set<Entry> entries = new HashSet<>();
        entries.add(new Entry(entity, entity.getClass(), id));
        saveEntityChanges(entries, getTime(), new QuickSearch(), author);
    }

    // History Entities with history tables

    @Override
    @Transactional
    public <T> void saveHistoryEntry(Collection<T> objects, String author) {
        final Set<Entry> entries = toEntries(objects);
        saveHistoryEntities(entries, DateTime.now(), new QuickSearch(), author);
    }

    @Override
    @Transactional
    public <T> void saveHistoryEntry(T entity, String author) {
        if (entity == null) {
            throw new EvolutionVersioningException("saveHistoryEntry(T entity) cannot accept null" +
                    " as entity parameter since there is no way of determining its class," +
                    " use saveHistoryEntry(T entity, Long id, Class<T> clazz) instead");
        }
        final Set<Entry> entries = new HashSet<>();
        entries.add(new Entry(entity));
        saveHistoryEntities(entries, getTime(), new QuickSearch(), author);
    }

    @Override
    @Transactional
    public <T> void saveHistoryEntry(T entity, Object id, String author) {
        if (entity == null) {
            throw new EvolutionVersioningException("saveHistoryEntry(T entity, Long id) cannot accept null" +
                    " as entity parameter since there is no way of determining its class," +
                    " use saveHistoryEntry(T entity, Long id, Class<T> clazz) instead");
        }
        final Set<Entry> entries = new HashSet<>();
        entries.add(new Entry(entity, entity.getClass(), id));
        saveHistoryEntities(entries, getTime(), new QuickSearch(), author);
    }

    @Override
    @Transactional
    public <T> void saveEntityChanges(T entity, Object id, Class<? extends T> clazz, String author) {
        final Set<Entry> entries = new HashSet<>();
        entries.add(new Entry(entity, clazz, id));
        saveEntityChanges(entries, getTime(), new QuickSearch(), author);
    }

    @Override
    @Transactional
    public <T> void deleteHistoryEntry(Object id, Class<T> clazz, String author) {
        final Set<Entry> entries = new HashSet<>();
        entries.add(new Entry(null, clazz, id));
        saveHistoryEntities(entries, getTime(), new QuickSearch(), author);
//        saveEntityChanges(entries, getTime(), new QuickSearch(), author);
    }

    /**
     * Bundle history parameters with the same name to lists.
     */
    private <T> Map<T, List<String>> joinParameters(List<HistoryParameter> parameters,
                                                    Function<HistoryParameter, T> joinFunction) {
        HashMap<T, List<String>> realValues = new HashMap<>();
        for (HistoryParameter parameter : parameters) {
            final T parameterName = joinFunction.apply(parameter);
            final List<String> values = realValues.computeIfAbsent(parameterName, k -> new ArrayList<>());
            values.add(parameter.getTextValue());
        }
        return realValues;
    }

    private <T, Q extends Enum<Q>> void setEntryField(T entry, Field field, List<String> values, DateTime time) {
        field.setAccessible(true);
        try {
            final Class<Q> type = (Class<Q>) field.getType();
            if (type.equals(List.class)) {
                List<Object> newList = new ArrayList<>();
                for (String s : values) {
                    final Class<?> elementClass = getListGenericClass(field);
                    newList.add(getHistoryVersion(elementClass, Long.valueOf(s), time));
                }
                field.set(entry, newList);
            } else if (values.isEmpty()) {
                field.set(entry, null);
            } else {
                if (values.size() > 1) {
                    throw new RuntimeException("More than one element! " + values.toString());
                }
                String value = values.get(0);
                if (PrimitiveTypes.contain(type) || type.isEnum()) {
                    field.set(entry, convertPrimitiveTypeOrEnum(value, type));
                } else {
                    final Object id = getIdFromAnnotation(entry);
                    field.set(entry, getHistoryVersion(type, id, time));
                }
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } finally {
            field.setAccessible(false);
        }
    }

    private <T, Q extends Enum<Q>> Object convertPrimitiveTypeOrEnum(String value, Class<T> type) {
        if (PrimitiveTypes.contain(type)) {
            return PrimitiveTypes.convert(type, value);
        } else if (type.isEnum()) {
            final List<Enum<Q>> enums = Arrays.asList(((Class<Enum<Q>>) type).getEnumConstants());
            final List<Enum<Q>> collect = enums.stream().filter(qEnum -> qEnum.toString().equals(value)).collect(Collectors.toList());
            return !collect.isEmpty() ? collect.get(0) : null;
        } else {
            throw new EvolutionVersioningException("Bad type conversion for " + value + ", of type " + type);
        }
    }

    private Class<?> getListGenericClass(Field listField) {
        ParameterizedType stringListType = (ParameterizedType) listField.getGenericType();
        return (Class<?>) stringListType.getActualTypeArguments()[0];
    }

    @Transactional
    public void saveEntityChanges(Collection<Entry> entries, DateTime time, QuickSearch savedEntries, String author) {
        final Set<Entry> allEntries = getAllConnectedEntries(entries);
        final List<ComplexHistoryEntity> historyEntities = getHistoryEntitiesForEntries(allEntries, time);
        compareAndSave(allEntries, historyEntities, savedEntries, time, author);
    }

    @Transactional
    public void saveHistoryEntities(Collection<Entry> entries, DateTime time, QuickSearch savedEntries, String author) {
        final Set<Entry> allEntries = getAllConnectedEntries(entries);
        saveToHistoryCacheTables(allEntries, savedEntries, time, author);
    }


    private Set<Entry> getAllConnectedEntries(Collection<Entry> entries) {
        final Set<Entry> result = new HashSet<>();
        final Queue<Entry> work = new LinkedList<>(entries);
        while (!work.isEmpty()) {
            final Entry element = work.poll();
            if (isVersioned(element.clazz) && !result.contains(element)) {
                result.add(element);
                final Set<Object> versionedParameters = getVersionedParameters(element.entry);
                work.addAll(toEntries(versionedParameters));
            }
        }
        return result;
    }

    private <T> Set<Entry> toEntries(Collection<T> values) {
        final Set<Entry> result = new HashSet<>();
        for (Object value : values) {
            result.add(new Entry(value));
        }
        return result;
    }

    private Set<Object> getVersionedParameters(Object entity) {
        final Set<Object> result = new HashSet<>();
        if (entity != null) {
            Class entityClass = entity.getClass();
            try {
                for (Field field : getAllDeclaredFields(entityClass)) {
                    field.setAccessible(true);
                    Class<?> fieldType = getFieldType(field, entityClass);
                    if (isStatic(field.getModifiers())) {
                        continue;
                    }
                    if (field.isAnnotationPresent(VersionedParameter.class)) {
                        final Object parameterValue;
                        parameterValue = field.get(entity);

                        if (fieldType.equals(List.class)) {
                            result.addAll((List<Object>) parameterValue);
                        } else {
                            if (parameterValue != null) result.add(parameterValue);
                        }
                    }
                    field.setAccessible(false);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private List<ComplexHistoryEntity> getHistoryEntitiesForEntries(Collection<Entry> entries, DateTime time) {
        final List<ComplexHistoryEntity> historyEntities = new ArrayList<>();
        for (Entry entry : entries) {
            final ComplexHistoryEntity e = new ComplexHistoryEntity();
            e.setEntityId(entry.id.toString());
//            e.setEntityIdClass(getIdClassFromAnnotation(entry.clazz).getType().getCanonicalName());
            e.setClazz(entry.clazz);
            historyEntities.add(e);
        }
        rawVersionService.fillHistoryEntities(historyEntities, time);
        return historyEntities;
    }

    /**
     * Compare entries with currently saved versions and save what changed to historyEntities and historyParameters.
     */
    private <Q> void compareAndSave(Collection<Entry> entries, List<ComplexHistoryEntity> entities, QuickSearch<String, Object, Long> savedEntries, DateTime time, String author) {
        Map<String, Map<String, ComplexHistoryEntity>> historyEntityMap = new HashMap<>();
        for (ComplexHistoryEntity entity : entities) {
            final String clazz = entity.getClazz().getCanonicalName();
            final String entityId = entity.getEntityId().toString();
            final Map<String, ComplexHistoryEntity> objectMap = historyEntityMap.computeIfAbsent(clazz, o -> new HashMap<>());
            objectMap.put(entityId, entity);
        }
        try {
            for (Entry entry : entries) {
                Class entityClass = entry.clazz;
                final Object entityId = entry.id;
                final Long saved = savedEntries.fetch(entityClass.getCanonicalName(), entityId);
                if (saved != null) return;
                final String canonicalName = fetchClassId(entityClass);
                final Map<String, ComplexHistoryEntity> objectMap = historyEntityMap.get(entityClass.getCanonicalName());
                ComplexHistoryEntity complexHistoryEntity = objectMap != null ? objectMap.get(entityId.toString()) : null;
                if (complexHistoryEntity == null) {
                    complexHistoryEntity = new ComplexHistoryEntity(entityClass, entityId, null, new ArrayList<>());
                }
                HistoryEntity historyEntity = complexHistoryEntity.getHistoryEntity();
                if (historyEntity == null) {
                    if (entry.entry != null) {
                        historyEntity = new HistoryEntity(entityId, canonicalName, author, timeToTimestamp(time), null);
                        complexHistoryEntity.setHistoryEntity(historyEntity);
                        entityManager.persist(historyEntity);
                    } else {
                        continue; // nothing changed
                    }
                }
                if (entry.entry == null) { // delete history entity
                    final Timestamp deletionTime = timeToTimestamp(time);
                    historyEntity.setDeleted(deletionTime);
                    final List<HistoryParameter> parameters = historyEntity.getParameters();
                    for (HistoryParameter parameter : parameters) {
                        if (parameter.getDeleted() == null) {
                            parameter.setDeleted(deletionTime);
                        }
                    }
                    continue;
                }
                final Long historyEntityId = historyEntity.getHistoryEntityId();
                savedEntries.save(canonicalName, entityId, historyEntityId);

                // todo change this so it works even after migration -- list of class fields might have changed, remove deprecated values
                for (Field field : getAllDeclaredFields(entityClass)) {
                    field.setAccessible(true);
                    Class<?> fieldType = getFieldType(field, entityClass);
                    if (isStatic(field.getModifiers())) {
                        continue;
                    }
                    final Object parameterValue = field.get(entry.entry);

                    List<Q> list;
                    Class<Q> elementClass;
                    if (fieldType.equals(List.class)) {
                        list = (List<Q>) parameterValue;
                        elementClass = (Class<Q>) getListGenericClass(field);
                    } else {
                        list = new ArrayList<>();
                        if (parameterValue != null) list.add((Q) parameterValue);
                        elementClass = (Class<Q>) fieldType;
                    }
                    persistListEntries(list, entityClass, entityId, complexHistoryEntity, elementClass, field, time, savedEntries, author);
                    field.setAccessible(false);
                }
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Save all entries to respective {@link cz.cvut.fit.evolution.versioning.Versioned.HistoryTable} entities.
     */
    private void saveToHistoryCacheTables(Collection<Entry> entries, QuickSearch<String, Object, Long> savedEntries, DateTime time, String author) {
        try {
            for (Entry entry : entries) {
                Class entityClass = entry.clazz;
                final Class<Versioned.HistoryTable> versionedCacheClass = getVersionedCacheClass(entityClass);
                final Versioned.HistoryTable historyTable = versionedCacheClass.newInstance();
                final Object entityId = entry.id;
                final String className = fetchClassId(entityClass);
                final Long saved = savedEntries.fetch(className, entityId);
                if (saved != null) return;
                final Timestamp deletionTime = timeToTimestamp(time);
                historyTable.setEntity(entry.entry, entry.id, deletionTime, author);
                entityManager.persist(historyTable);
                savedEntries.save(className, entityId, -1L);
            }
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    // save versioned element (disguised as list) or a list of elements
    private <T> void persistListEntries(List<T> list, Class clazz, Object entityId, ComplexHistoryEntity complexHistoryEntity,
                                        Class<T> elementClass, Field field, DateTime time, QuickSearch savedEntries, String author) {
        try {
            // first, we fetch the old version of our entity
            final List<HistoryParameter> parameters = complexHistoryEntity.getHistoryParameters();
            final Set<Object> historyListIds = new HashSet<>();
            final Map<Object, HistoryParameter> historyIdMap = new HashMap<>();
            // we search the old entity for parameter entries which are of our desired parameter
            for (HistoryParameter parameter : parameters) {
                if (parameter.getParameterName().equals(field.getName())) {
                    T id; // it might be primitive type, enum, or a foreign key to another HistoryEntry
                    if (PrimitiveTypes.contain(elementClass) || elementClass.isEnum()) { // primitive type or an enum
                        id = (T) convertPrimitiveTypeOrEnum(parameter.getTextValue(), elementClass);
                    } else { // foreign key
                        final Class<?> idClass = getIdClassFromAnnotation(elementClass).getType();
                        if (PrimitiveTypes.contain(idClass)) {
                            id = (T) PrimitiveTypes.convert(idClass, parameter.getTextValue());
                        } else {
                            throw new EvolutionVersioningException("Unknown entry type " + elementClass + " for value of " + parameter);
                        }
                    }
                    historyListIds.add(id);
                    historyIdMap.put(id, parameter);
                }
            }
            // now we compare the old version with the new ... comparison depends on type of the value
            if (PrimitiveTypes.contain(elementClass) || elementClass.isEnum()) {
                for (T listElement : list) {
                    if (!historyListIds.contains(listElement)) {
                        saveOneValue(listElement, field, elementClass, complexHistoryEntity.getHistoryEntity(), savedEntries, time, author);
                    } else {
                        historyListIds.remove(listElement);
                    }
                }
            } else {
                for (T listElement : list) {
                    final Object childId = getIdFromAnnotation(listElement);
                    if (!historyListIds.contains(childId)) {
                        saveOneValue(childId, field, elementClass, complexHistoryEntity.getHistoryEntity(), savedEntries, time, author);
                    } else {
                        historyListIds.remove(childId);
                    }
                }
            }
            for (Object removedHistId : historyListIds) {
                final HistoryParameter historyParameter = historyIdMap.get(removedHistId);
                historyParameter.setDeleted(timeToTimestamp(time));
                if (historyParameter.getDeleted().equals(historyParameter.getCreated())) {
                    complexHistoryEntity.getHistoryEntity().getParameters().remove(historyParameter);
                }
            }
        } catch (RuntimeException e) {
            throw new EvolutionVersioningException("While persisting values of " + list + ", "
                    + "of class " + clazz + ", entity id " + entityId + ", field " + field, e);
        }
    }

    private String fetchClassId(Class<?> clazz) {
        return clazz.getCanonicalName();
    }

    private void saveOneValue(Object element, Field field, Class<?> type, HistoryEntity historyEntity,
                              QuickSearch savedEntries, DateTime time, String author) {
        String parameterName = field.getName();
        final String parameterType = fetchClassId(type);
        String parameterValue = fetchValue(element, savedEntries, time);
        createParameter(parameterName, parameterType, author, historyEntity, time, parameterValue);
    }

    private String fetchValue(Object element, QuickSearch savedEntries, DateTime time) {
        try {
            String parameterValue = null;
            final Class<?> type = element.getClass();
            if (PrimitiveTypes.contain(type) || type.isEnum()) {
                parameterValue = element.toString();
            } else {
                final Object elementId = getIdFromAnnotation(element);
                parameterValue = elementId != null ? elementId.toString() : null;
            }
            return parameterValue;
        } catch (RuntimeException e) {
            throw new EvolutionVersioningException("While fetching the value of " + element, e);
        }
    }

    private Object getIdFromAnnotation(Object obj) {
        Class clazz = obj.getClass();
        try {
            final Field field = getIdClassFromAnnotation(clazz);
            field.setAccessible(true);
            if (field.isAnnotationPresent(Id.class)) {
                return field.get(obj);
            }
            field.setAccessible(false);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Did not find Id in class " + obj.getClass());
    }

    private Field getIdClassFromAnnotation(Class<?> clazz) {
        while (clazz != null) {
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(Id.class)) {
                    return field;
                }
                field.setAccessible(false);
            }
            clazz = clazz.getSuperclass();
        }
        throw new RuntimeException("Did not find Id in class " + clazz);
    }

    private void createParameter(String name, String type, String author, HistoryEntity historyEntity, DateTime creationTime, String value) {
        final Timestamp created = timeToTimestamp(creationTime);
        final HistoryParameter parameter = new HistoryParameter(name, value, type, author, historyEntity, created, null);
        historyEntity.getParameters().add(parameter);
        entityManager.persist(parameter);
    }

    private boolean isVersioned(Class clazz) {
        return clazz.isAnnotationPresent(Versioned.class);
    }

    private Class<Versioned.HistoryTable> getVersionedCacheClass(Class clazz) {
        if (isVersioned(clazz)) {
            final Versioned annotation = (Versioned) clazz.getAnnotation(Versioned.class);
            if (annotation != null) {
                // todo check that the class extends Versioned.HistoryTable
                return (Class<Versioned.HistoryTable>) annotation.value();
            }
        }
        return null;
    }

    private <T> T justGetThatEntity(Class<T> clazz, Object id) {
        return entityManager.find(clazz, id);
    }

    private class Entry {
        Object entry;
        Class<?> clazz;
        Object id;

        public Entry(Object entry) {
            this.entry = entry;
            this.clazz = entry.getClass();
            this.id = getIdFromAnnotation(entry);
        }

        public Entry(Object entry, Class<?> clazz, Object id) {
            this.entry = entry;
            this.clazz = clazz;
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Entry entry = (Entry) o;

            if (clazz != null ? !clazz.equals(entry.clazz) : entry.clazz != null) return false;
            return id != null ? id.equals(entry.id) : entry.id == null;
        }

        @Override
        public int hashCode() {
            int result = clazz != null ? clazz.hashCode() : 0;
            result = 31 * result + (id != null ? id.hashCode() : 0);
            return result;
        }
    }

}
