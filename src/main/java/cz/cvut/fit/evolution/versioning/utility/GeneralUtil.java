package cz.cvut.fit.evolution.versioning.utility;

import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class GeneralUtil {

    public static <T> boolean listEq(Collection<T> a, Collection<T> b) {
        if (a == b) return true;
        if (a == null || b == null) return false;
        List<T> q = new ArrayList<>();
        q.addAll(a);
        for (T t : b) {
            if (!q.contains(t)) return false;
            q.remove(t);
        }
        return q.isEmpty();
    }

    public static <I, O> O flatten(O collection, List<I> list, Collect<O, I> collector) {
        for (I i : list) collector.collect(collection, i);
        return collection;
    }

    public static <T> T copyObject(T old, Class<T> clazz) throws IllegalAccessException, InstantiationException {
        if (old == null) return null;
        T entry = clazz.newInstance();
        for (Field field : getAllDeclaredFields(clazz)) {
            field.setAccessible(true);
            field.set(entry, field.get(old));
            field.setAccessible(false);
        }
        return entry;
    }

    public static List<Field> getAllDeclaredFields(Class clazz) {
        final ArrayList<Field> fields = new ArrayList<>();
        while (clazz != null) {
            final Field[] declaredFields = clazz.getDeclaredFields();
            fields.addAll(Arrays.asList(declaredFields));
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

    public static Class<?> getFieldType(Field field, Class<?> clazz) {
        return (Class<?>) getRawFieldType(field, clazz).type;
    }

    // https://stackoverflow.com/questions/1868333/how-can-i-determine-the-type-of-a-generic-field-in-java
    private static TypeInfo getRawFieldType(Field field, Class<?> clazz) {
        TypeInfo type = new TypeInfo(null, null);
        if (field.getGenericType() instanceof TypeVariable<?>) {
            TypeVariable<?> genericType = (TypeVariable<?>) field.getGenericType();
            Class<?> superClazz = clazz.getSuperclass();

            if (clazz.getGenericSuperclass() instanceof ParameterizedType) {
                ParameterizedType paramType = (ParameterizedType) clazz.getGenericSuperclass();
                TypeVariable<?>[] superTypeParameters = superClazz.getTypeParameters();
                if (!Object.class.equals(superClazz)) {
                    if (field.getDeclaringClass().equals(superClazz)) {
                        // this is the root class an starting point for this search
                        type.name = genericType;
                        type.type = null;
                    } else {
                        type = getRawFieldType(field, superClazz);
                    }
                }
                if (type.type == null || type.type instanceof TypeVariable<?>) {
                    // lookup if type is not found or type needs a lookup in current concrete class
                    for (int j = 0; j < superClazz.getTypeParameters().length; ++j) {
                        TypeVariable<?> superTypeParam = superTypeParameters[j];
                        if (type.name.equals(superTypeParam)) {
                            type.type = paramType.getActualTypeArguments()[j];
                            Type[] typeParameters = clazz.getTypeParameters();
                            if (typeParameters.length > 0) {
                                for (Type typeParam : typeParameters) {
                                    TypeVariable<?> objectOfComparison = superTypeParam;
                                    if (type.type instanceof TypeVariable<?>) {
                                        objectOfComparison = (TypeVariable<?>) type.type;
                                    }
                                    if (objectOfComparison.getName().equals(((TypeVariable<?>) typeParam).getName())) {
                                        type.name = typeParam;
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        } else {
            type.type = field.getGenericType();
        }
        if(type.type instanceof ParameterizedTypeImpl){
            type.type = ((ParameterizedTypeImpl) type.type).getRawType();
        }

        return type;
    }

    @FunctionalInterface
    public interface Collect<T, A> {
        void collect(T t, A a);
    }

    protected static class TypeInfo {
        Type type;
        Type name;

        TypeInfo(Type type, Type name) {
            this.type = type;
            this.name = name;
        }
    }
}
