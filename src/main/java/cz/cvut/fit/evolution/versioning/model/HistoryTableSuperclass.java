package cz.cvut.fit.evolution.versioning.model;

import cz.cvut.fit.evolution.versioning.Versioned;

import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@MappedSuperclass
public abstract class HistoryTableSuperclass<T> implements Versioned.HistoryTable<T> {

    private Timestamp created;

    private Boolean deleted;

    private String author;

    @Override
    public void setEntity(T entity, Object id, Timestamp timestamp, String author) {
        this.deleted = (entity == null);
        this.created = timestamp;
        this.author = author;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
