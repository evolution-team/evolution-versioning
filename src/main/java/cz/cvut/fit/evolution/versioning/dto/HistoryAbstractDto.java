package cz.cvut.fit.evolution.versioning.dto;

public interface HistoryAbstractDto<T> {

    void fromEntity(T entity);

    T toEntity();
}