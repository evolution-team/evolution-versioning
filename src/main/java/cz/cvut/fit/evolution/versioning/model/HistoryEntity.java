package cz.cvut.fit.evolution.versioning.model;

import cz.cvut.fit.evolution.versioning.utility.GeneralUtil;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"ENTITY_ID", "CLAZZ"}))
public class HistoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long historyEntityId;

    private String entityIdClass;

    private String entityId;

    private String clazz;

    @OneToMany(mappedBy = "historyEntity")
    private List<HistoryParameter> parameters = new ArrayList<>();

    private String author;

    private Timestamp created;

    private Timestamp deleted;

    public HistoryEntity() {
    }

    public HistoryEntity(Object entityId, String clazz, String author, Timestamp created, Timestamp deleted) {
        this.entityIdClass = entityId.getClass().getCanonicalName();
        this.entityId = entityId.toString();
        this.clazz = clazz;
        this.author = author;
        this.created = created;
        this.deleted = deleted;
    }

    public Long getHistoryEntityId() {
        return historyEntityId;
    }

    public void setHistoryEntityId(Long historyEntityId) {
        this.historyEntityId = historyEntityId;
    }

    public String getEntityIdClass() {
        return entityIdClass;
    }

    public void setEntityIdClass(String entityIdClass) {
        this.entityIdClass = entityIdClass;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public List<HistoryParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<HistoryParameter> parameters) {
        this.parameters = parameters;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getDeleted() {
        return deleted;
    }

    public void setDeleted(Timestamp deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "HistoryEntity{" +
                "historyEntityId=" + historyEntityId +
                ", entityId=" + entityId +
                ", clazz='" + clazz + '\'' +
                ", parameters=" + parameters +
                ", author='" + author + '\'' +
                ", created=" + created +
                ", deleted=" + deleted +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoryEntity that = (HistoryEntity) o;

        if (historyEntityId != null ? !historyEntityId.equals(that.historyEntityId) : that.historyEntityId != null)
            return false;
        if (entityId != null ? !entityId.equals(that.entityId) : that.entityId != null) return false;
        if (clazz != null ? !clazz.equals(that.clazz) : that.clazz != null) return false;
        if (!GeneralUtil.listEq(parameters, that.parameters)) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        return deleted != null ? deleted.equals(that.deleted) : that.deleted == null;
    }

    @Override
    public int hashCode() {
        int result = historyEntityId != null ? historyEntityId.hashCode() : 0;
        result = 31 * result + (entityId != null ? entityId.hashCode() : 0);
        result = 31 * result + (clazz != null ? clazz.hashCode() : 0);
        result = 31 * result + (parameters != null ? parameters.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        return result;
    }
}