package cz.cvut.fit.evolution.versioning.service;


import cz.cvut.fit.evolution.versioning.Versioned;
import cz.cvut.fit.evolution.versioning.model.HistoryParameter;

/**
 * @author Václav Blažej
 * @see Versioned
 * @since 1.0
 */
public interface MigrateVersionService {

    <T> void renameParameter(Class<T> clazz, String from, String to);

    <T> void changeParameterType(Class<T> clazz, String parameterName, Class to);

    void changeParameter(HistoryParameter pattern, HistoryParameter target);

    <T> void changeParameterValue(Class<T> clazz, String parameterName, T originalValue, T newValue);

    void migrateClass(Class from, Class to);
}
