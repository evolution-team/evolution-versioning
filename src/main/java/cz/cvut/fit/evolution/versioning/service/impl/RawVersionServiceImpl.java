package cz.cvut.fit.evolution.versioning.service.impl;


import cz.cvut.fit.dbcore.service.AbstractModelService;
import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.HistoryParameter;
import cz.cvut.fit.evolution.versioning.service.RawVersionService;
import cz.cvut.fit.evolution.versioning.utility.ComplexHistoryEntity;
import cz.cvut.fit.evolution.versioning.utility.PrimitiveTypes;
import cz.cvut.fit.evolution.versioning.utility.QuickSearch;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.*;
import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

import static cz.cvut.fit.evolution.versioning.utility.TimeUtil.timeToTimestamp;
import static org.hibernate.criterion.Restrictions.*;

public class RawVersionServiceImpl extends AbstractModelService implements RawVersionService {

    @Override
    @Transactional
    public HistoryEntity getHistoryEntity(Class clazz, Object id) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(HistoryEntity.class, "entity");
        criteria.add(eq("entity.clazz", clazz.getCanonicalName()));
        criteria.add(eq("entity.entityId", id.toString()));
        criteria.createAlias("entity.parameters", "parameters");
        final Object o = criteria.uniqueResult();
        final HistoryEntity historyEntity = (HistoryEntity) o;
        if (historyEntity != null) {
            Hibernate.initialize(historyEntity.getParameters());
        }
        return historyEntity;
    }

    @Override
    @Transactional(readOnly = true)
    public void fillHistoryEntities(List<ComplexHistoryEntity> entities, DateTime time) {
        final QuickSearch<String, Object, ComplexHistoryEntity> quickSearch = new QuickSearch<>();
        for (ComplexHistoryEntity entity : entities) {
            quickSearch.save(entity.getClazz().getCanonicalName(), entity.getEntityId(), entity);
        }
        final List<Disjunction> disjunctions = new ArrayList<>();
        { // cut the query to avoid overflow otherwise due to huge query size
            Disjunction disjunction = Restrictions.disjunction();
            int entitiesSize = entities.size();
            int cutSize = 2000;
            for (int i = 0; i < entitiesSize; i++) {
                final ComplexHistoryEntity entity = entities.get(i);
                String clazz = entity.getClazz().getCanonicalName();
                Object entityId = entity.getEntityId();
                disjunction.add(Restrictions.conjunction(eq("entity.clazz", clazz),
//                    eq("entity.entityIdClass", entityId.getClass().getCanonicalName()),
                        eq("entity.entityId", entityId)));
                if (i % cutSize == cutSize - 1 || i == entitiesSize - 1) {
                    disjunctions.add(disjunction);
                    disjunction = Restrictions.disjunction();
                }
            }
        }
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(HistoryEntity.class, "entity");
        final List<HistoryEntity> list = new ArrayList<>();
        {
            final Timestamp queryTime = timeToTimestamp(time);
            final SimpleExpression biggerCreationTime = le("entity.created", queryTime);
            final Criterion nullDeletionTime = isNull("entity.deleted");
            final SimpleExpression smallerDeletionTime = gt("entity.deleted", queryTime);
            final LogicalExpression timeCriteria = and(biggerCreationTime, or(nullDeletionTime, smallerDeletionTime));
            for (Disjunction disjunction : disjunctions) {
                criteria.add(Restrictions.conjunction(disjunction, timeCriteria));
                list.addAll(criteria.list());
            }
        }
        final Map<Long, ComplexHistoryEntity> idToComplexEntities = new HashMap<>();
        for (HistoryEntity historyEntity : list) {
            final ComplexHistoryEntity fetch = quickSearch.fetch(historyEntity.getClazz(), historyEntity.getEntityId());
            fetch.setHistoryEntity(historyEntity);
            idToComplexEntities.put(historyEntity.getHistoryEntityId(), fetch);
        }

        final Criteria parCriteria = hibernateCriteriaCreator.createCriteria(HistoryParameter.class, "parameter");
        final Timestamp queryTime = timeToTimestamp(time);
        final SimpleExpression biggerCreationTime = le("parameter.created", queryTime);
        final Criterion nullDeletionTime = isNull("parameter.deleted");
        final SimpleExpression smallerDeletionTime = gt("parameter.deleted", queryTime);
        parCriteria.add(and(biggerCreationTime, or(nullDeletionTime, smallerDeletionTime)));
        parCriteria.createAlias("parameter.historyEntity", "historyEntity"); // todo optimize
        final Set<Long> values = idToComplexEntities.keySet();
        if (!values.isEmpty()) {
            parCriteria.add(in("historyEntity.historyEntityId", values));
        }
        final List<HistoryParameter> parameters = parCriteria.list();
        for (HistoryParameter parameter : parameters) {
            final Long historyEntityId = parameter.getHistoryEntity().getHistoryEntityId();
            final ComplexHistoryEntity complexHistoryEntity = idToComplexEntities.get(historyEntityId);
            if (complexHistoryEntity != null) {
                complexHistoryEntity.getHistoryParameters().add(parameter);
            }
        }
    }

    @Override
    @Transactional
    public List<HistoryEntity> getAllHistoryEntities(Class clazz) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(HistoryEntity.class, "entity");
        criteria.add(eq("entity.clazz", clazz.getCanonicalName()));
        return (List<HistoryEntity>) criteria.list();
    }

    @Override
    @Transactional
    public List<HistoryParameter> getHistoryParameters(Class clazz, Object id) {
        return this.getHistoryParameters(clazz, id, DateTime.now());
    }

    /**
     * @return object representing the inner state of the entity at some point in time
     */
    @Override
    @Transactional(readOnly = true)
    public List<HistoryParameter> getHistoryParameters(Class clazz, Object id, DateTime time) {
        final Criteria criteria = hibernateCriteriaCreator.createCriteria(HistoryEntity.class, "entity");
        criteria.add(eq("entity.clazz", clazz.getCanonicalName()));
        criteria.add(eq("entity.entityId", PrimitiveTypes.convert(id)));
        final HistoryEntity historyEntity = (HistoryEntity) criteria.uniqueResult();
        if (historyEntity == null) return null;

        final Timestamp queryTime = timeToTimestamp(time);
        final Criteria parCriteria = hibernateCriteriaCreator.createCriteria(HistoryParameter.class, "parameter");
        parCriteria.createAlias("parameter.historyEntity", "entity");
        parCriteria.add(Restrictions.eq("entity.historyEntityId", historyEntity.getHistoryEntityId()));
        final SimpleExpression biggerCreationTime = le("parameter.created", queryTime);
        final Criterion nullDeletionTime = isNull("parameter.deleted");
        final SimpleExpression smallerDeletionTime = gt("parameter.deleted", queryTime);
        parCriteria.add(and(biggerCreationTime, or(nullDeletionTime, smallerDeletionTime)));

        return (List<HistoryParameter>) parCriteria.list();
    }

    @Override
    @Transactional
    public List<HistoryParameter> getAllHistoryParameters() {
        final Criteria parCriteria = hibernateCriteriaCreator.createCriteria(HistoryParameter.class, "parameter");
        return (List<HistoryParameter>) parCriteria.list();
    }

}
