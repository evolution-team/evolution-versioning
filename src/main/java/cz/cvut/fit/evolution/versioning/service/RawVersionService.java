package cz.cvut.fit.evolution.versioning.service;


import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.HistoryParameter;
import cz.cvut.fit.evolution.versioning.utility.ComplexHistoryEntity;
import org.joda.time.DateTime;

import java.util.List;

/**
 * The RawVersionService provides database query interface to entities which are used for versioning.
 *
 * @author Václav Blažej
 * @see HistoryEntity
 * @see HistoryParameter
 * @since 1.0
 */
public interface RawVersionService {

    HistoryEntity getHistoryEntity(Class clazz, Object id);

    void fillHistoryEntities(List<ComplexHistoryEntity> entities, DateTime time);

    List<HistoryEntity> getAllHistoryEntities(Class clazz);

    List<HistoryParameter> getHistoryParameters(Class clazz, Object id);

    List<HistoryParameter> getHistoryParameters(Class clazz, Object id, DateTime time);

    List<HistoryParameter> getAllHistoryParameters();
}
