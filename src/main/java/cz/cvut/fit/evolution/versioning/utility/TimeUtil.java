package cz.cvut.fit.evolution.versioning.utility;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.sql.Timestamp;

public class TimeUtil {

    private static String TIME_ZONE = "Europe/Prague";

    public static void setTimeZone(String timeZone) {
        TIME_ZONE = timeZone;
    }

    public static DateTime timeFromTimestamp(Timestamp timestamp) {
        if (timestamp == null) return null;
        return new DateTime(timestamp, DateTimeZone.forID(TIME_ZONE));
    }

    public static Timestamp timeToTimestamp(DateTime dateTime) {
        if (dateTime == null) return null;
        return new Timestamp(dateTime.getMillis());
    }

    public static boolean same(Timestamp a, Timestamp b) {
        return a == b || a != null && b != null && Long.compare(timeFromTimestamp(a).getMillis(), timeFromTimestamp(b).getMillis()) == 0;
    }
}
