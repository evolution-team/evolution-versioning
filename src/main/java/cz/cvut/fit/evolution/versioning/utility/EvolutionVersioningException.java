package cz.cvut.fit.evolution.versioning.utility;

public class EvolutionVersioningException extends RuntimeException {

    public EvolutionVersioningException() {
    }

    public EvolutionVersioningException(String message) {
        super(message);
    }

    public EvolutionVersioningException(String message, Throwable cause) {
        super(message, cause);
    }

    public EvolutionVersioningException(Throwable cause) {
        super(cause);
    }

    public EvolutionVersioningException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
