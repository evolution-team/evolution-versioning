package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.service.VersionService;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.sql.Timestamp;

/**
 * Annotation Versioned is required to allow history tracking.
 * <p>
 * Migrating an entity and adding Versioned annotation will enable tracking.
 * When the first save is made the database creates a HistoryEntity and persists all parameters as HistoryParameter.
 * <p>
 * Removing the Versioned annotation will disable tracking.
 * todo should write a deletionTime to HistoryParameters
 *
 * @author Václav Blažej
 * @see VersionService
 * @since 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Versioned {
    Class value() default MissingTableInAnnotation.class;

    interface HistoryTable<T> {

        void setEntity(T entity, Object id, Timestamp timestamp, String author);

        T getEntity();
    }

    class MissingTableInAnnotation implements HistoryTable {

        @Override
        public void setEntity(Object entity, Object id, Timestamp timestamp, String author) {
        }

        @Override
        public Object getEntity() {
            return null;
        }
    }
}
