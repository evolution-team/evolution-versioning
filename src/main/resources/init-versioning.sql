CREATE TABLE HISTORY_ENTITY (
  HISTORY_ENTITY_ID BIGSERIAL PRIMARY KEY NOT NULL,
  ENTITY_ID_CLASS   TEXT                  NOT NULL,
  ENTITY_ID         TEXT                  NOT NULL,
  CLAZZ             TEXT                  NOT NULL,
  AUTHOR            TEXT                  NOT NULL,
  CREATED           TIMESTAMP             NOT NULL,
  DELETED           TIMESTAMP
);
CREATE TABLE HISTORY_PARAMETER (
  HISTORY_PARAMETER_ID BIGSERIAL PRIMARY KEY NOT NULL,
  PARAMETER_NAME       TEXT                  NOT NULL,
  TEXT_VALUE           TEXT                  NOT NULL,
  TYPE                 TEXT                  NOT NULL,
  AUTHOR               TEXT                  NOT NULL,
  HISTORY_ENTITY_ID    BIGSERIAL             NOT NULL REFERENCES HISTORY_ENTITY,
  CREATED              TIMESTAMP             NOT NULL,
  DELETED              TIMESTAMP
);
