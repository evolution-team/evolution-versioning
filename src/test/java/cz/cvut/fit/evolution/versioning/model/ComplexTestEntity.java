package cz.cvut.fit.evolution.versioning.model;

import cz.cvut.fit.evolution.versioning.Versioned;
import cz.cvut.fit.evolution.versioning.VersionedListener;
import cz.cvut.fit.evolution.versioning.VersionedParameter;
import cz.cvut.fit.evolution.versioning.utility.GeneralUtil;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Versioned
@EntityListeners(VersionedListener.class)
public class ComplexTestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long complexTestEntityId;

    @VersionedParameter
    @OneToMany(cascade = CascadeType.MERGE, orphanRemoval = true, mappedBy = "complexTestEntity")
    private List<TestEntity> entities = new ArrayList<>();

    @OneToOne
    private TestEntity entity;

    @Enumerated(EnumType.STRING)
    private TestEnum testEnum;

    public Long getComplexTestEntityId() {
        return complexTestEntityId;
    }

    public void setComplexTestEntityId(Long complexTestEntityId) {
        this.complexTestEntityId = complexTestEntityId;
    }

    public List<TestEntity> getEntities() {
        return entities;
    }

    public void setEntities(List<TestEntity> entities) {
        this.entities = entities;
    }

    public TestEntity getEntity() {
        return entity;
    }

    public void setEntity(TestEntity entity) {
        this.entity = entity;
    }

    public TestEnum getTestEnum() {
        return testEnum;
    }

    public void setTestEnum(TestEnum testEnum) {
        this.testEnum = testEnum;
    }

    @Override
    public String toString() {
        return "ComplexTestEntity{" +
                "complexTestEntityId=" + complexTestEntityId +
                ", entities=" + entities +
                ", entity=" + entity +
                ", testEnum=" + testEnum +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ComplexTestEntity that = (ComplexTestEntity) o;

        if (complexTestEntityId != null ? !complexTestEntityId.equals(that.complexTestEntityId) : that.complexTestEntityId != null)
            return false;
        if (!GeneralUtil.listEq(this.getEntities(), that.getEntities())) return false;
        if (entity != null ? !entity.equals(that.entity) : that.entity != null) return false;
        return testEnum == that.testEnum;
    }

    @Override
    public int hashCode() {
        int result = complexTestEntityId != null ? complexTestEntityId.hashCode() : 0;
        result = 31 * result + (entities != null ? entities.hashCode() : 0);
        result = 31 * result + (entity != null ? entity.hashCode() : 0);
        result = 31 * result + (testEnum != null ? testEnum.hashCode() : 0);
        return result;
    }
}
