package cz.cvut.fit.evolution.versioning.model;

import cz.cvut.fit.evolution.versioning.Versioned;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Versioned
public class StringIdTestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String testEntityId;

    private String name;

    private Long value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getTestEntityId() {
        return testEntityId;
    }

    public void setTestEntityId(String testEntityId) {
        this.testEntityId = testEntityId;
    }

    @Override
    public String toString() {
        return "TestEntity{" +
                "id=" + testEntityId +
                ", name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StringIdTestEntity that = (StringIdTestEntity) o;

        if (testEntityId != null ? !testEntityId.equals(that.testEntityId) : that.testEntityId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = testEntityId != null ? testEntityId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}


