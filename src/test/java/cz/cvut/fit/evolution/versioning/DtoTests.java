package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.dto.HistoryEntityDto;
import cz.cvut.fit.evolution.versioning.dto.HistoryParameterDto;
import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.HistoryParameter;
import cz.cvut.fit.evolution.versioning.rules.TmpDatabaseRule;
import cz.cvut.fit.evolution.versioning.service.TestEntityService;
import cz.cvut.fit.evolution.versioning.service.VersionService;
import cz.cvut.fit.evolution.versioning.utility.TimeUtil;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class DtoTests {

    @Autowired
    @Rule
    public TmpDatabaseRule tmpDatabaseRule;

    @Autowired
    private VersionService versionService;

    @Autowired
    private TestEntityService testEntityService;

    @PersistenceContext
    private EntityManager entityManager;


    @Test
    public void transformToAndFromDto() {
        final HistoryEntity oldEntity = new HistoryEntity(40L, Long.class.getCanonicalName(), "hello kitty", null, null);
        final List<HistoryParameter> parameters = oldEntity.getParameters();
        final Timestamp timestamp = TimeUtil.timeToTimestamp(DateTime.now());
        final HistoryParameter parameter = new HistoryParameter("parName", "my text", "STRING", "author", oldEntity, timestamp, timestamp);
        parameters.add(parameter);

        final HistoryEntityDto historyEntityDto = new HistoryEntityDto();
        historyEntityDto.fromEntity(oldEntity);
        final HistoryEntity newEntity = historyEntityDto.toEntity();

        Assert.assertNotNull(newEntity);
        Assert.assertEquals(newEntity, oldEntity);
    }

    @Test
    public void transformFromAndToDto() {
        final HistoryEntityDto oldDto = new HistoryEntityDto(40L, Long.class.getCanonicalName(), "hello kitty", null, null);
        final List<HistoryParameterDto> parameters = oldDto.getParameters();
        final DateTime time = DateTime.now();
        final HistoryParameterDto parameterDto = new HistoryParameterDto("parName", "my text", "STRING", "author", oldDto, time, time);
        parameters.add(parameterDto);

        final HistoryEntityDto historyEntityDto = new HistoryEntityDto();
        historyEntityDto.fromEntity(oldDto.toEntity());

        Assert.assertNotNull(historyEntityDto);
        Assert.assertEquals(historyEntityDto, oldDto);
    }


}
