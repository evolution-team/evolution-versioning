package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.model.TestEntity;
import cz.cvut.fit.evolution.versioning.model.generic.NumberTestEntity;
import cz.cvut.fit.evolution.versioning.rules.TmpDatabaseRule;
import cz.cvut.fit.evolution.versioning.service.RawVersionService;
import cz.cvut.fit.evolution.versioning.service.VersionService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class SpecialCasesTests {

    @Autowired
    @Rule
    public TmpDatabaseRule tmpDatabaseRule;

    @Autowired
    private VersionService versionService;

    @Autowired
    private RawVersionService rawVersionService;

    private void pause() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saveChildOfGenericEntity() {
        final NumberTestEntity entity = new NumberTestEntity();
        entity.setTestEntityId(3L);
        entity.setValue(3.14159265);
        versionService.saveEntityChanges(entity, "test");
    }

    @Test
    public void manyEntriesTimeTest() {
        // measurements in h2 db
        //
        // original
        // 24.68 for 1500 entries
        //
        // upgraded - bulk to one select query and split when query is huge
        //  0.8 for 1500 entries
        // 10.64 for 30000 entries
        // 35.75 for 60000 entries
        final List<Object> entities = new ArrayList<>();
        final int count = 1500;
        for (int i = 0; i < count; i++) {
            final TestEntity e = new TestEntity();
            e.setTestEntityId((long) i);
            e.setValue((long) i);
            e.setName("test" + i);
            entities.add(e);
        }
        versionService.saveEntityChanges(entities, "test");
    }

    @Test
    public void manyQueriesTimeTest() {
        // measurements in h2 db
        //
        // original
        //
        // upgraded - bulk to one select query and split when query is huge
        final List<Object> entities = new ArrayList<>();
        final int count = 3000;
        final int inserts = count / 2;
        final int updates = count / 2;
        for (int i = 0; i < inserts; i++) {
            final TestEntity e = new TestEntity();
            e.setTestEntityId((long) i);
            e.setValue((long) i);
            e.setName("test" + i);
            entities.add(e);
        }
        versionService.saveEntityChanges(entities, "test");
    }

}
