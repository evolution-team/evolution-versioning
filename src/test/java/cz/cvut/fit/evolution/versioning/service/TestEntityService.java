package cz.cvut.fit.evolution.versioning.service;

import cz.cvut.fit.evolution.versioning.model.ComplexTestEntity;
import cz.cvut.fit.evolution.versioning.model.TestEntity;


public interface TestEntityService {

    void saveTestEntity(TestEntity entity);

    void saveTestEntity(ComplexTestEntity entity);

}
