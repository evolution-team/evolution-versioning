package cz.cvut.fit.evolution.versioning.model.generic;

import cz.cvut.fit.evolution.versioning.Versioned;

import javax.persistence.*;

@Versioned
@MappedSuperclass
public class GenericTestEntity<T> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long testEntityId;

    private T value;

    public Long getTestEntityId() {
        return testEntityId;
    }

    public void setTestEntityId(Long testEntityId) {
        this.testEntityId = testEntityId;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
