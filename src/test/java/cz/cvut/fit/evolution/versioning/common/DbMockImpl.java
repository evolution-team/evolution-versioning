package cz.cvut.fit.evolution.versioning.common;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class DbMockImpl implements DbMock {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void initDatabase() {
    }

    @Override
    @Transactional
    public void deleteDatabase() {
        entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY FALSE").executeUpdate();
        final List<Object[]> res = entityManager.createNativeQuery("SHOW TABLES").getResultList();
        for (Object[] re : res) {
            entityManager.createNativeQuery("TRUNCATE TABLE " + re[0]).executeUpdate();
        }
        entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY TRUE").executeUpdate();
    }
}
