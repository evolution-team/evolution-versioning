package cz.cvut.fit.evolution.versioning.rules;

import cz.cvut.fit.evolution.versioning.common.DbMock;
import org.junit.rules.ExternalResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@ContextConfiguration(locations = {"classpath:test-database.xml"})
public class TmpDatabaseRule extends ExternalResource {

    @Autowired
    private DbMock dbMock;

    @Override
    protected void before() throws Throwable {
        dbMock.initDatabase();
    }

    @Override
    protected void after() {
        dbMock.deleteDatabase();
    }
}
