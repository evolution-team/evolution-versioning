package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.model.TestWithTableEntity;
import cz.cvut.fit.evolution.versioning.rules.TmpDatabaseRule;
import cz.cvut.fit.evolution.versioning.service.VersionService;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Iterator;
import java.util.Map;

import static cz.cvut.fit.evolution.versioning.common.TestUtil.buildCachedEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class CachedVersionServiceTests {


    @Autowired
    @Rule
    public TmpDatabaseRule tmpDatabaseRule;

    @Autowired
    private VersionService versionService;

    private void pause() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void historyCacheEntity() {
        final TestWithTableEntity entity = buildCachedEntity(4L, 100L, "test");
        final Long testEntityId = entity.getTestEntityId();
        final Class<? extends TestWithTableEntity> clazz = entity.getClass();
        versionService.saveEntityChanges(entity, testEntityId, clazz, "test");
        pause();
        versionService.saveEntityChanges(null, testEntityId, clazz, "test");

        final Map<DateTime, ? extends TestWithTableEntity> allVersions = versionService.getAllHistoryVersions(clazz, testEntityId);
        Assert.assertNotNull(allVersions);
        Assert.assertEquals(2, allVersions.size());
        final Iterator<? extends TestWithTableEntity> iterator = allVersions.values().iterator();
        Assert.assertNotNull(iterator.next());
        Assert.assertNull(iterator.next());
    }


}
