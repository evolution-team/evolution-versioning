package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.TestEntity;
import cz.cvut.fit.evolution.versioning.rules.TmpDatabaseRule;
import cz.cvut.fit.evolution.versioning.service.RawVersionService;
import cz.cvut.fit.evolution.versioning.service.VersionService;
import cz.cvut.fit.evolution.versioning.utility.HibernateCriteriaCreator;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Map;

import static cz.cvut.fit.evolution.versioning.common.TestUtil.buildTestEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class TransactionTests {

    @Autowired
    @Rule
    public TmpDatabaseRule tmpDatabaseRule;

    @Autowired
    private VersionService versionService;

    @Autowired
    private RawVersionService rawVersionService;

    @Autowired
    private HibernateCriteriaCreator criteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    private void pause() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Transactional
    public void fewerEntitiesInTransaction() {
        TestEntity entity = buildTestEntity(3L, 100L, "Martin");
        versionService.saveEntityChanges(entity, "test");
        pause();
        entity.setValue(200L);
        versionService.saveEntityChanges(entity, "test");
        pause();
        entity.setValue(300L);
        versionService.saveEntityChanges(entity, "test");
        pause();

        final Map<DateTime, TestEntity> versions = versionService.getAllHistoryVersions(TestEntity.class, 3L);
        Assert.assertNotNull(versions);
        Assert.assertEquals(1, versions.size());

        final HistoryEntity rawHistoryEntity = rawVersionService.getHistoryEntity(TestEntity.class, 3L);
        Assert.assertEquals(3, rawHistoryEntity.getParameters().size());
    }

    @Test
    @Transactional
    public void transactionCommit() {
        final Session session = entityManager.unwrap(Session.class);
        final Transaction transaction = session.getTransaction();
        final TestEntity test = buildTestEntity(4L, 123L, "test");
        versionService.saveEntityChanges(test, "test");
        transaction.commit();
        final TestEntity historyVersion = versionService.getHistoryVersion(TestEntity.class, 4L);
        Assert.assertNotNull(historyVersion);
        Assert.assertEquals(test, historyVersion);
    }

    @Test
    @Transactional
    public void transactionRollback() {
        final Session session = entityManager.unwrap(Session.class);
        final Transaction transaction = session.getTransaction();
        final TestEntity test = buildTestEntity(4L, 123L, "test");
        versionService.saveEntityChanges(test, "test");
        transaction.rollback();
        final TestEntity historyVersion = versionService.getHistoryVersion(TestEntity.class, 4L);
        Assert.assertNull(historyVersion);
    }

}
