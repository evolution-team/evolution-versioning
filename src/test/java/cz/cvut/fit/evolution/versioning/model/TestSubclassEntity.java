package cz.cvut.fit.evolution.versioning.model;

import cz.cvut.fit.evolution.versioning.Versioned;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Versioned
@Entity
@PrimaryKeyJoinColumn
public class TestSubclassEntity extends TestEntity {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "TestSubclassEntity{" +
                "text='" + text + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TestSubclassEntity that = (TestSubclassEntity) o;

        return text != null ? text.equals(that.text) : that.text == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }
}


