package cz.cvut.fit.evolution.versioning.common;

public interface DbMock {

    void initDatabase();

    void deleteDatabase();
}
