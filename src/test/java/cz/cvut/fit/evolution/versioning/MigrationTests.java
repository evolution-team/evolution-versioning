package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.HistoryParameter;
import cz.cvut.fit.evolution.versioning.model.TestEntity;
import cz.cvut.fit.evolution.versioning.rules.TmpDatabaseRule;
import cz.cvut.fit.evolution.versioning.service.MigrateVersionService;
import cz.cvut.fit.evolution.versioning.service.RawVersionService;
import cz.cvut.fit.evolution.versioning.service.VersionService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.stream.Collectors;

import static cz.cvut.fit.evolution.versioning.common.TestUtil.buildTestEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class MigrationTests {

    @Autowired
    @Rule
    public TmpDatabaseRule tmpDatabaseRule;

    @Autowired
    private VersionService versionService;

    @Autowired
    private RawVersionService rawVersionService;

    @Autowired
    private MigrateVersionService migrateVersionService;

    @Test
    public void changeParameterName() {
        final TestEntity test = buildTestEntity(2L, 22L, "blazewan");
        versionService.saveEntityChanges(test, "test");

        migrateVersionService.renameParameter(TestEntity.class, "name", "username");

        final HistoryEntity historyEntity = rawVersionService.getHistoryEntity(TestEntity.class, 2L);
        Assert.assertNotNull(historyEntity);
        final List<HistoryParameter> parameters = historyEntity.getParameters();
        final List<String> collect = parameters.stream().map(HistoryParameter::getParameterName).collect(Collectors.toList());
        Assert.assertTrue(collect.contains("testEntityId"));
        Assert.assertTrue(collect.contains("value"));
        Assert.assertFalse(collect.contains("name"));
        Assert.assertTrue(collect.contains("username"));
    }

    @Test
    public void changeParameterType() {
        final TestEntity test = buildTestEntity(2L, 22L, "432");
        versionService.saveEntityChanges(test, "test");
        migrateVersionService.changeParameterType(TestEntity.class, "name", Long.class);

        final HistoryEntity historyEntity = rawVersionService.getHistoryEntity(TestEntity.class, 2L);
        Assert.assertNotNull(historyEntity);
        final List<HistoryParameter> parameters = historyEntity.getParameters();
        final List<String> collect = parameters.stream().map(HistoryParameter::getType).collect(Collectors.toList());
        Assert.assertTrue(collect.contains(Long.class.getCanonicalName()));
        Assert.assertFalse(collect.contains(String.class.getCanonicalName()));
        final List<Long> values = parameters.stream().map(parameter -> Long.valueOf(parameter.getTextValue())).collect(Collectors.toList());
        Assert.assertTrue(values.contains(2L));
        Assert.assertTrue(values.contains(22L));
        Assert.assertTrue(values.contains(432L));
    }

    @Test
    public void advancedMigration() {
        versionService.saveEntityChanges(buildTestEntity(2L, 22L, "abc"), "test");
        versionService.saveEntityChanges(buildTestEntity(3L, 33L, "test"), "test");
        versionService.saveEntityChanges(buildTestEntity(4L, 22L, "zdenek"), "test");
        versionService.saveEntityChanges(buildTestEntity(5L, 55L, "martin"), "test");

        final HistoryParameter pattern = new HistoryParameter();
        pattern.setTextValue("22");
        final HistoryParameter target = new HistoryParameter();
        target.setTextValue("1000");
        migrateVersionService.changeParameter(pattern, target);

        final List<HistoryParameter> parameters = rawVersionService.getAllHistoryParameters();
        Assert.assertNotNull(parameters);
        Assert.assertEquals(12, parameters.size());
        final List<String> collect = parameters.stream().map(HistoryParameter::getTextValue).collect(Collectors.toList());
        Assert.assertTrue(collect.contains("1000"));
        Assert.assertFalse(collect.contains("22"));
    }

}
