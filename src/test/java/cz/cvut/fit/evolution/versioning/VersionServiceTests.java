package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.model.*;
import cz.cvut.fit.evolution.versioning.rules.TmpDatabaseRule;
import cz.cvut.fit.evolution.versioning.service.RawVersionService;
import cz.cvut.fit.evolution.versioning.service.VersionService;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static cz.cvut.fit.evolution.versioning.common.TestUtil.buildComplexTestEntity;
import static cz.cvut.fit.evolution.versioning.common.TestUtil.buildTestEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class VersionServiceTests {

    @Autowired
    @Rule
    public TmpDatabaseRule tmpDatabaseRule;

    @Autowired
    private VersionService versionService;

    @Autowired
    private RawVersionService rawVersionService;

    private void pause() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void listCreationTest() {
        final ComplexTestEntity entity = new ComplexTestEntity();
        entity.setComplexTestEntityId(2L);
        final List<TestEntity> entities = entity.getEntities();
        final TestEntity pavel = buildTestEntity(4L, 100L, "Pavel");
        final TestEntity marek = buildTestEntity(5L, 200L, "Marek");
        final TestEntity roman = buildTestEntity(6L, 300L, "Roman");
        entities.add(pavel);
        entities.add(marek);
        versionService.saveEntityChanges(entity, "test");
        pause();
        entities.add(roman);
        versionService.saveEntityChanges(entity, "test");
        pause();
        final DateTime mid = DateTime.now();
        pause();
        entities.remove(marek);
        versionService.saveEntityChanges(entity, "test");
        pause();

        final ComplexTestEntity historyVersion = versionService.getHistoryVersion(ComplexTestEntity.class, 2L);
        Assert.assertNotNull(historyVersion);
        final List<TestEntity> list = historyVersion.getEntities();
        Assert.assertEquals(2, list.size());
        Assert.assertTrue(list.contains(pavel));
        Assert.assertTrue(list.contains(roman));
        Assert.assertFalse(list.contains(marek));

        final ComplexTestEntity midHistoryVersion = versionService.getHistoryVersion(ComplexTestEntity.class, 2L, mid);
        Assert.assertNotNull(midHistoryVersion);
        final List<TestEntity> midList = midHistoryVersion.getEntities();

        Assert.assertEquals(3, midList.size());
        Assert.assertTrue(midList.contains(pavel));
        Assert.assertTrue(midList.contains(roman));
        Assert.assertTrue(midList.contains(marek));
    }

    @Test
    public void subclassVersioning() {
        final TestSubclassEntity entity = new TestSubclassEntity();
        entity.setText("TEXT");
        entity.setValue(100L);
        entity.setTestEntityId(99L);
        entity.setName("NAME");
        versionService.saveEntityChanges(entity, "test");

        final TestSubclassEntity historyVersion = versionService.getHistoryVersion(TestSubclassEntity.class, 99L);
        Assert.assertNotNull(historyVersion);
        Assert.assertEquals(entity, historyVersion);
    }

    @Test
    public void testCreationTest() {
        final ComplexTestEntity entity = new ComplexTestEntity();
        entity.setComplexTestEntityId(2L);
        final List<TestEntity> entities = entity.getEntities();
        final TestEntity pavel = buildTestEntity(4L, 100L, "Pavel");
        final TestEntity marek = buildTestEntity(5L, 200L, "Marek");
        final TestEntity roman = buildTestEntity(6L, 300L, "Roman");
        entities.add(pavel);
        entities.add(marek);
        versionService.saveEntityChanges(entity, "test");
        pause();
        entities.add(roman);
        versionService.saveEntityChanges(entity, "test");
        pause();
        entities.remove(marek);
        versionService.saveEntityChanges(entity, "test");
        pause();

        final Map<DateTime, ComplexTestEntity> historyVersions = versionService.getAllHistoryVersions(ComplexTestEntity.class, 2L);
        Assert.assertEquals(3, historyVersions.size());
        final Iterator<Map.Entry<DateTime, ComplexTestEntity>> iterator = historyVersions.entrySet().iterator();

        entities.clear();
        entities.add(pavel);
        entities.add(marek);
        final Map.Entry<DateTime, ComplexTestEntity> first = iterator.next();
        Assert.assertEquals(entity, first.getValue());
        entities.add(roman);
        final Map.Entry<DateTime, ComplexTestEntity> second = iterator.next();
        Assert.assertEquals(entity, second.getValue());
        entities.remove(marek);
        final Map.Entry<DateTime, ComplexTestEntity> third = iterator.next();
        Assert.assertEquals(entity, third.getValue());
    }

    @Test
    public void betweenHistoryCommitsTest() {
        final ComplexTestEntity entity = new ComplexTestEntity();
        entity.setComplexTestEntityId(2L);
        final List<TestEntity> entities = entity.getEntities();
        final TestEntity pavel = buildTestEntity(4L, 100L, "Pavel");
        final TestEntity marek = buildTestEntity(5L, 200L, "Marek");
        final TestEntity roman = buildTestEntity(6L, 300L, "Roman");
        entities.add(pavel);
        entities.add(marek);
        versionService.saveEntityChanges(entity, "test");
        final DateTime firstTime = DateTime.now();
        pause();
        entities.add(roman);
        versionService.saveEntityChanges(entity, "test");
        final DateTime secondTime = DateTime.now();
        pause();
        entities.remove(marek);
        versionService.saveEntityChanges(entity, "test");
        final DateTime thirdTime = DateTime.now();
        pause();

        entities.clear();

        final ComplexTestEntity first = versionService.getHistoryVersion(ComplexTestEntity.class, 2L, firstTime);
        Assert.assertNotNull(first);
        entities.add(pavel);
        entities.add(marek);
        Assert.assertEquals(entity, first);

        final ComplexTestEntity second = versionService.getHistoryVersion(ComplexTestEntity.class, 2L, secondTime);
        entities.add(roman);
        Assert.assertEquals(entity, second);

        final ComplexTestEntity third = versionService.getHistoryVersion(ComplexTestEntity.class, 2L, thirdTime);
        entities.remove(marek);
        Assert.assertEquals(entity, third);
    }

    @Test
    public void nonVersionedEnum() {
        final ComplexTestEntity complexTestEntity = buildComplexTestEntity(3L);
        complexTestEntity.setTestEnum(TestEnum.TODAY);
        versionService.saveEntityChanges(complexTestEntity, "test");

        final ComplexTestEntity historyVersion = versionService.getHistoryVersion(ComplexTestEntity.class, 3L);
        Assert.assertNotNull(historyVersion);
        Assert.assertEquals(complexTestEntity, historyVersion);
    }

    @Test
    public void multipleEntitiesDetails() {
        TestEntity first = buildTestEntity(2L, 22L, "first");
        versionService.saveEntityChanges(first, "test");
        TestEntity second = buildTestEntity(3L, 33L, "second");
        versionService.saveEntityChanges(second, "test");

        final List<HistoryEntity> entities = rawVersionService.getAllHistoryEntities(TestEntity.class);
        Assert.assertNotNull(entities);
        Assert.assertEquals(2, entities.size());
        for (HistoryEntity entity : entities) {
            Assert.assertNotNull(entity);
        }
    }

    @Test
    public void changingEnumValues() {
        final ComplexTestEntity complexTestEntity = buildComplexTestEntity(3L);
        complexTestEntity.setTestEnum(TestEnum.TODAY);
        versionService.saveEntityChanges(complexTestEntity, "test");
        complexTestEntity.setTestEnum(TestEnum.TOMORROW);
        versionService.saveEntityChanges(complexTestEntity, "test");

        final ComplexTestEntity historyVersion = versionService.getHistoryVersion(ComplexTestEntity.class, 3L);
        Assert.assertNotNull(historyVersion);
        Assert.assertEquals(complexTestEntity, historyVersion);
    }

    @Test
    public void noEnumDuplicates() {
        final ComplexTestEntity complexTestEntity = buildComplexTestEntity(3L);
        complexTestEntity.setTestEnum(TestEnum.TODAY);
        versionService.saveEntityChanges(complexTestEntity, "test");
        pause();
        complexTestEntity.setTestEnum(TestEnum.TODAY);
        versionService.saveEntityChanges(complexTestEntity, "test");
        versionService.saveEntityChanges(complexTestEntity, "test");
        pause();
        versionService.saveEntityChanges(complexTestEntity, "test");

        final List<HistoryParameter> allHistoryParameters = rawVersionService.getAllHistoryParameters();
        Assert.assertNotNull(allHistoryParameters);
        Assert.assertEquals(2, allHistoryParameters.size()); // id and enum
    }

    @Test
    public void deleteEntity() {
        final TestEntity entity = buildTestEntity(4L, 100L, "test");
        final Long testEntityId = entity.getTestEntityId();
        final Class<? extends TestEntity> clazz = entity.getClass();
        versionService.saveEntityChanges(entity, testEntityId, clazz, "test");
        pause();
        versionService.saveEntityChanges(null, testEntityId, clazz, "test");

        final Map<DateTime, ? extends TestEntity> allVersions = versionService.getAllHistoryVersions(clazz, testEntityId);
        Assert.assertNotNull(allVersions);
        Assert.assertEquals(2, allVersions.size());
        final Iterator<? extends TestEntity> iterator = allVersions.values().iterator();
        Assert.assertNotNull(iterator.next());
        Assert.assertNull(iterator.next());
    }

    @Test
    public void stringId() {
        final StringIdTestEntity stringIdTestEntity = new StringIdTestEntity();
        stringIdTestEntity.setName("name");
        stringIdTestEntity.setTestEntityId("entityId");
        stringIdTestEntity.setValue(100L);
        versionService.saveEntityChanges(stringIdTestEntity, "test");
        pause();
        final StringIdTestEntity entity = versionService.getHistoryVersion(StringIdTestEntity.class, "entityId");
        Assert.assertNotNull(entity);
        Assert.assertEquals(stringIdTestEntity, entity);
    }

}
