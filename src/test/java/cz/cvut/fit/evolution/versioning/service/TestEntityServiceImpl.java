package cz.cvut.fit.evolution.versioning.service;

import cz.cvut.fit.evolution.versioning.model.ComplexTestEntity;
import cz.cvut.fit.evolution.versioning.model.TestEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class TestEntityServiceImpl implements TestEntityService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void saveTestEntity(TestEntity entity) {
        entityManager.persist(entity);
    }

    @Override
    @Transactional
    public void saveTestEntity(ComplexTestEntity entity) {
        for (TestEntity testEntity : entity.getEntities()) {
            entityManager.persist(testEntity);
        }
        entityManager.persist(entity);
    }

}
