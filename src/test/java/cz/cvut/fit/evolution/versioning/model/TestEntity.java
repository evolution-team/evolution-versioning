package cz.cvut.fit.evolution.versioning.model;

import cz.cvut.fit.evolution.versioning.Versioned;
import cz.cvut.fit.evolution.versioning.VersionedListener;

import javax.persistence.*;

@Versioned
@EntityListeners(VersionedListener.class)
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class TestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long testEntityId;

    private String name;

    private Long value;

    @ManyToOne
    @JoinColumn(name = "complex_test_entity_id")
    private ComplexTestEntity complexTestEntity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Long getTestEntityId() {
        return testEntityId;
    }

    public void setTestEntityId(Long testEntityId) {
        this.testEntityId = testEntityId;
    }

    public ComplexTestEntity getComplexTestEntity() {
        return complexTestEntity;
    }

    public void setComplexTestEntity(ComplexTestEntity complexTestEntity) {
        this.complexTestEntity = complexTestEntity;
    }

    @Override
    public String toString() {
        return "TestEntity{" +
                "id=" + testEntityId +
                ", name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestEntity that = (TestEntity) o;

        if (testEntityId != null ? !testEntityId.equals(that.testEntityId) : that.testEntityId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = testEntityId != null ? testEntityId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
