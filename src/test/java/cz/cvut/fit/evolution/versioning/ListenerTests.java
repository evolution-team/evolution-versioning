package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.model.ComplexTestEntity;
import cz.cvut.fit.evolution.versioning.model.TestEntity;
import cz.cvut.fit.evolution.versioning.rules.TmpDatabaseRule;
import cz.cvut.fit.evolution.versioning.service.TestEntityService;
import cz.cvut.fit.evolution.versioning.service.VersionService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static cz.cvut.fit.evolution.versioning.common.TestUtil.buildTestEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class ListenerTests {

    @Autowired
    @Rule
    public TmpDatabaseRule tmpDatabaseRule;

    @Autowired
    private VersionService versionService;

    @Autowired
    private TestEntityService testEntityService;

    @PersistenceContext
    private EntityManager entityManager;


    @Test
    public void simpleAutosave() {
        final TestEntity entity = new TestEntity();
        entity.setName("Master Yoda");
        entity.setValue(134L);
        testEntityService.saveTestEntity(entity);

        final TestEntity historyVersion = versionService.getHistoryVersion(TestEntity.class, entity.getTestEntityId());
        Assert.assertNotNull(historyVersion);
    }

    @Test
    @Transactional
    public void cascadeEntitySave() {
        final ComplexTestEntity entity = new ComplexTestEntity();
        final List<TestEntity> entities = entity.getEntities();
        final TestEntity smallEntity = buildTestEntity(5L, 100L, "test name");
        smallEntity.setComplexTestEntity(entity);
        entities.add(smallEntity);
        entityManager.persist(entity);

        final TestEntity historySmallEntity = versionService.getHistoryVersion(TestEntity.class, smallEntity.getTestEntityId());
        Assert.assertNotNull(historySmallEntity);
        Assert.assertEquals(smallEntity, historySmallEntity);

        final ComplexTestEntity histEntity = versionService.getHistoryVersion(ComplexTestEntity.class, entity.getComplexTestEntityId());
        Assert.assertNotNull(histEntity);
        Assert.assertEquals(entity, histEntity);
    }

}
