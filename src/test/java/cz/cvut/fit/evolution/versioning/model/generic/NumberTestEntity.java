package cz.cvut.fit.evolution.versioning.model.generic;

import cz.cvut.fit.evolution.versioning.Versioned;

import javax.persistence.Entity;

@Versioned
@Entity
public class NumberTestEntity extends GenericTestEntity<Double> {

}
