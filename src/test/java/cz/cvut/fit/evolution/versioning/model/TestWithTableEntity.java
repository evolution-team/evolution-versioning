package cz.cvut.fit.evolution.versioning.model;

import cz.cvut.fit.evolution.versioning.Versioned;
import cz.cvut.fit.evolution.versioning.VersionedListener;

import javax.persistence.*;

@Versioned(HistoryTestWithTableEntity.class)
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class TestWithTableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long testEntityId;

    private String name;

    private Long value;

    public Long getTestEntityId() {
        return testEntityId;
    }

    public void setTestEntityId(Long testEntityId) {
        this.testEntityId = testEntityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
