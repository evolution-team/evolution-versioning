package cz.cvut.fit.evolution.versioning.common;

import cz.cvut.fit.evolution.versioning.model.ComplexTestEntity;
import cz.cvut.fit.evolution.versioning.model.TestEntity;
import cz.cvut.fit.evolution.versioning.model.TestNotVersionedEntity;
import cz.cvut.fit.evolution.versioning.model.TestWithTableEntity;

public class TestUtil {

    public static TestEntity buildTestEntity(Long id, Long value, String name) {
        final TestEntity entity = new TestEntity();
        entity.setTestEntityId(id);
        entity.setValue(value);
        entity.setName(name);
        return entity;
    }

    public static TestWithTableEntity buildCachedEntity(Long id, Long value, String name) {
        final TestWithTableEntity entity = new TestWithTableEntity();
        entity.setTestEntityId(id);
        entity.setValue(value);
        entity.setName(name);
        return entity;
    }

    public static TestNotVersionedEntity buildTestNotVersionedEntity(Long id, Long value, String name) {
        final TestNotVersionedEntity entity = new TestNotVersionedEntity();
        entity.setId(id);
        entity.setValue(value);
        entity.setName(name);
        return entity;
    }

    public static ComplexTestEntity buildComplexTestEntity(Long id) {
        final ComplexTestEntity complexTestEntity = new ComplexTestEntity();
        complexTestEntity.setComplexTestEntityId(id);
        return complexTestEntity;
    }
}
