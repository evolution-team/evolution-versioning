package cz.cvut.fit.evolution.versioning.model;

import cz.cvut.fit.evolution.versioning.Versioned;

import javax.persistence.*;
import java.sql.Timestamp;

@Versioned(HistoryTestWithTableEntity.class)
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class HistoryTestWithTableEntity extends HistoryTableSuperclass<TestWithTableEntity> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long historyTestEntityId;

    private Long testEntityId;

    private String name;

    private Long value;

    @Override
    public void setEntity(TestWithTableEntity entity, Object id, Timestamp timestamp, String author) {
        super.setEntity(entity, id, timestamp, author);
        setTestEntityId(entity.getTestEntityId());
        setName(entity.getName());
        setValue(entity.getValue());
    }

    @Override
    public TestWithTableEntity getEntity() {
        final TestWithTableEntity entity = new TestWithTableEntity();
        entity.setName(getName());
        entity.setValue(getValue());
        entity.setTestEntityId(getTestEntityId());
        return entity;
    }


    public Long getHistoryTestEntityId() {
        return historyTestEntityId;
    }

    public void setHistoryTestEntityId(Long historyTestEntityId) {
        this.historyTestEntityId = historyTestEntityId;
    }

    public Long getTestEntityId() {
        return testEntityId;
    }

    public void setTestEntityId(Long testEntityId) {
        this.testEntityId = testEntityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
