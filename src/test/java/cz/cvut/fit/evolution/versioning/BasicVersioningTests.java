package cz.cvut.fit.evolution.versioning;

import cz.cvut.fit.evolution.versioning.model.ComplexTestEntity;
import cz.cvut.fit.evolution.versioning.model.HistoryEntity;
import cz.cvut.fit.evolution.versioning.model.TestEntity;
import cz.cvut.fit.evolution.versioning.model.TestNotVersionedEntity;
import cz.cvut.fit.evolution.versioning.rules.TmpDatabaseRule;
import cz.cvut.fit.evolution.versioning.service.RawVersionService;
import cz.cvut.fit.evolution.versioning.service.VersionService;
import cz.cvut.fit.evolution.versioning.utility.HibernateCriteriaCreator;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Map;

import static cz.cvut.fit.evolution.versioning.common.TestUtil.buildTestEntity;
import static cz.cvut.fit.evolution.versioning.common.TestUtil.buildTestNotVersionedEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class BasicVersioningTests {

    @Autowired
    @Rule
    public TmpDatabaseRule tmpDatabaseRule;

    @Autowired
    private VersionService versionService;

    @Autowired
    private RawVersionService rawVersionService;

    @Autowired
    private HibernateCriteriaCreator criteriaCreator;

    @PersistenceContext
    private EntityManager entityManager;

    private void pause() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saveAndLoad() {
        TestEntity entity = buildTestEntity(2L, 1L, "name");
        versionService.saveEntityChanges(entity, 2L, "test");

        final TestEntity historyVersion = versionService.getHistoryVersion(TestEntity.class, 2L);
        Assert.assertEquals(entity, historyVersion);
    }

    @Test
    public void saveAndLoadMore() {
        TestEntity first = buildTestEntity(2L, 22L, "first");
        versionService.saveEntityChanges(first, "test");
        TestEntity second = buildTestEntity(3L, 33L, "second");
        versionService.saveEntityChanges(second, "test");

        final TestEntity historyFirst = versionService.getHistoryVersion(TestEntity.class, 2L);
        Assert.assertEquals(first, historyFirst);
        final TestEntity historySecond = versionService.getHistoryVersion(TestEntity.class, 3L);
        Assert.assertEquals(second, historySecond);
    }

    @Test
    public void saveMultipleTimes() {
        TestEntity entity = buildTestEntity(3L, 100L, "Martin");
        versionService.saveEntityChanges(entity, "test");
        pause();
        entity.setValue(200L);
        versionService.saveEntityChanges(entity, "test");
        pause();
        entity.setValue(300L);
        versionService.saveEntityChanges(entity, "test");
        pause();

        final Map<DateTime, TestEntity> versions = versionService.getAllHistoryVersions(TestEntity.class, 3L);
        Assert.assertNotNull(versions);
        Assert.assertEquals(3L, versions.size());

        final HistoryEntity rawHistoryEntity = rawVersionService.getHistoryEntity(TestEntity.class, 3L);
        Assert.assertEquals(5, rawHistoryEntity.getParameters().size());
    }

    @Test
    public void changingRedundantly() {
        TestEntity entity = buildTestEntity(3L, 100L, "Martin");
        versionService.saveEntityChanges(entity, "test");
        pause();
        entity.setValue(100L);
        versionService.saveEntityChanges(entity, "test");
        pause();
        entity.setName("Martin");
        versionService.saveEntityChanges(entity, "test");

        final Map<DateTime, TestEntity> versions = versionService.getAllHistoryVersions(TestEntity.class, 3L);
        Assert.assertNotNull(versions);
        Assert.assertEquals(1L, versions.size());

        final HistoryEntity rawHistoryEntity = rawVersionService.getHistoryEntity(TestEntity.class, 3L);
        Assert.assertEquals(3, rawHistoryEntity.getParameters().size());
    }

    @Test
    public void getOlderVersion() {
        final TestEntity original = buildTestEntity(1L, 11L, "First");
        final TestEntity entity = buildTestEntity(1L, 11L, "First");
        versionService.saveEntityChanges(entity, "test");
        pause();
        entity.setName("Second");
        DateTime old = DateTime.now();
        pause();
        entity.setValue(30L);
        versionService.saveEntityChanges(entity, "test");
        pause();

        final TestEntity historyVersion = versionService.getHistoryVersion(TestEntity.class, 1L, old);
        Assert.assertEquals(original, historyVersion);
    }

    @Test
    public void getNewerVersion() {
        final TestEntity entity = buildTestEntity(1L, 11L, "First");
        versionService.saveEntityChanges(entity, "test");
        pause();
        entity.setName("Second");
        entity.setValue(30L);
        versionService.saveEntityChanges(entity, "test");
        pause();

        final TestEntity historyVersion = versionService.getHistoryVersion(TestEntity.class, 1L);
        Assert.assertEquals(entity, historyVersion);
    }

    @Test
    public void complexStructure() {
        final ComplexTestEntity entity = new ComplexTestEntity();
        entity.setComplexTestEntityId(1L);
        final TestEntity pavel = buildTestEntity(1L, 100L, "Pavel");
        versionService.saveEntityChanges(pavel, "test");
        entity.setEntity(pavel);
        versionService.saveEntityChanges(entity, "test");
        pause();

        final TestEntity small = versionService.getHistoryVersion(TestEntity.class, 1L);
        Assert.assertNotNull(small);
        Assert.assertEquals(new Long(100L), small.getValue());

        final ComplexTestEntity historyVersion = versionService.getHistoryVersion(ComplexTestEntity.class, 1L);
        Assert.assertNotNull(historyVersion);
        Assert.assertEquals(new Long(1L), historyVersion.getComplexTestEntityId());

        final TestEntity ent = historyVersion.getEntity();
        Assert.assertNotNull(ent);
        Assert.assertEquals(pavel, ent);
    }

    @Test
    public void entityMissing() {
        final TestEntity test = buildTestEntity(3L, 43L, "test");
        versionService.saveEntityChanges(test, "test");

        final TestEntity historyVersion = versionService.getHistoryVersion(TestEntity.class, 2L); // id mismatch
        Assert.assertNull(historyVersion);
    }

    @Test
    @Transactional
    public void nonVersionedEntityReplace() {
        final TestNotVersionedEntity test = buildTestNotVersionedEntity(null, 43L, "test");
        entityManager.persist(test);

        final TestNotVersionedEntity historyVersion = versionService.getHistoryVersion(TestNotVersionedEntity.class, test.getId());
        Assert.assertEquals(test, historyVersion);
    }

    @Test
    public void checkAuthor(){
        final TestEntity authoredEntity = buildTestEntity(3L, 100L, "hello author");
        versionService.saveEntityChanges(authoredEntity, "testaut3");

        final HistoryEntity historyEntity = rawVersionService.getHistoryEntity(TestEntity.class, 3L);
        Assert.assertEquals("testaut3", historyEntity.getAuthor());
    }

}
